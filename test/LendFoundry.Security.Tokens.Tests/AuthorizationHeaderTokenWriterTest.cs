using System;
using Microsoft.AspNet.Http;
using Moq;
using Xunit;

namespace LendFoundry.Security.Tokens.Tests
{
    public class AuthorizationHeaderTokenWriterTest
    {
        [Fact]
        public void ShouldFailWithoutToken()
        {
            var accessor = Mock.Of<IHttpContextAccessor>();
            var writer = new AuthorizationHeaderTokenWriter(accessor);
            Assert.Throws<ArgumentNullException>(() => writer.Write(null));
        }

        [Fact]
        public void ShouldFailWithoutHttpContext()
        {
            var token = Mock.Of<IToken>();
            var accessor = Mock.Of<IHttpContextAccessor>();
            var writer = new AuthorizationHeaderTokenWriter(accessor);
            Assert.Throws<Exception>(() => writer.Write(token));
        }

        [Fact]
        public void ShouldWriteTokenInAuthorizationHeader()
        {
            var expectedToken = "token";
            var token = Mock.Of<IToken>(t => t.Value == expectedToken);
            var headers = new Mock<IHeaderDictionary>();
            var request = Mock.Of<HttpRequest>(r => r.Headers == headers.Object);
            var context = Mock.Of<HttpContext>(c => c.Request == request);
            var accessor = Mock.Of<IHttpContextAccessor>(a => a.HttpContext == context);

            var writer = new AuthorizationHeaderTokenWriter(accessor);
            writer.Write(token);

            headers.Verify(d => d.Add("Authorization", new[] { string.Concat("Bearer ", expectedToken) }));
        }

        [Fact]
        public void ShouldOverwriteTokenInAuthorizationHeader()
        {
            var expectedToken = "token";
            var token = Mock.Of<IToken>(t => t.Value == expectedToken);
            var headers = new Mock<IHeaderDictionary>();
            headers.Setup(h => h.ContainsKey("Authorization")).Returns(true);

            var request = Mock.Of<HttpRequest>(r => r.Headers == headers.Object);
            var context = Mock.Of<HttpContext>(c => c.Request == request);
            var accessor = Mock.Of<IHttpContextAccessor>(a => a.HttpContext == context);

            var writer = new AuthorizationHeaderTokenWriter(accessor);
            writer.Write(token);

            headers.VerifySet(h => h["Authorization"] = string.Concat("Bearer ", expectedToken));
        }
    }
}