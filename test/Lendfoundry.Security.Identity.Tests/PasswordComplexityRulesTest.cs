﻿using LendFoundry.Security.Identity;
using Xunit;

namespace Lendfoundry.Security.Identity.Tests
{
    public class PasswordComplexityRulesTest
    {
        private IPasswordComplexityRules Rules { get; } = new PasswordComplexityRules();

        [Fact]
        public void NullPasswordDoesNotComply()
        {
            Assert.False(Rules.Complies(null));
        }

        [Fact]
        public void BlankPasswordDoesNotComply()
        {
            Assert.False(Rules.Complies("        "));
        }

        [Fact]
        public void DoesNotComplyWhenItHasLessThanEightCharacters()
        {
            Assert.False(Rules.Complies("a9b#cDe"));
        }

        [Fact]
        public void DoesNotComplyWhenItDoesNotContainAnyNumber()
        {
            Assert.False(Rules.Complies("a_b#cDef"));
        }

        [Fact]
        public void DoesNotComplyWhenItDoesNotContainAnySymbol()
        {
            Assert.False(Rules.Complies("a9bacDef"));
        }

        [Fact]
        public void DoesNotComplyWhenItDoesNotContainAnyCapitalLetter()
        {
            Assert.False(Rules.Complies("a9b#cdef"));
        }

        [Fact]
        public void CompliesWhenItHasAtLeastEightCharactersOneNumberOneSymbolAndOneCapitalLetter()
        {
            Assert.True(Rules.Complies("a9b#cDef"));
        }

        [Fact]
        public void CompliesWhenItHasMoreThanOneOfTheRequiredCharacters()
        {
            Assert.True(Rules.Complies("a9b23c#$dEFgh"));
        }
    }
}
