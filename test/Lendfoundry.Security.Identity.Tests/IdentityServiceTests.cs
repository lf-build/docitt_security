﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lendfoundry.Security.Identity.Tests.Fakes;
using LendFoundry.Configuration.Client;
using LendFoundry.Email;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Identity;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using Xunit;
using LendFoundry.Security.Identity.Mongo;
using LendFoundry.Foundation.Date;

namespace Lendfoundry.Security.Identity.Tests
{
    public class IdentityServiceTests
    {
        [Fact]
        public async Task LoginWithInvalidCredentials()
        {
            await Assert.ThrowsAsync<InvalidUserOrPasswordException>(async () =>
            {
                var service = await BuildService();
                await service.Login(new LoginRequest() { Username = "notexist", Password="notexist", Portal = "notExist"});
            });
        }

        [Fact]
        public async Task LoginWithValidCredentials()
        {
            var service = await BuildService(new List<IUser> { GetUserWithVpSales() });
            var result = await service.Login(new LoginRequest() { Username = "sanchari.gb", Portal = "vpOfSales", Password = "test123" });
            //parse the token result
            var token = GetTokenHandler().Parse(result.Token);
            Assert.NotNull(token.Scope);
            Assert.NotNull(Array.Find(token.Scope, element => element.Contains("vpOfSales")));
        }

        [Fact]
        public async Task ChangePasswordWithInvalidCurrentPasswordThrowsException()
        {
            var service = await BuildService(new List<IUser> {GetUserWithVpSales()});
            var result = await service.Login(new LoginRequest() { Username = "sanchari.gb", Password = "test123", Portal = "vpOfSales"});
            
            var token = GetTokenHandler().Parse(result.Token);

            Assert.NotNull(token.Scope);
            Assert.NotNull(Array.Find(token.Scope, element => element.Contains("vpOfSales")));
            await Assert.ThrowsAsync<InvalidUserOrPasswordException>(() => service.ChangePassword("sanchari.gb", "notvalid", "newPassword"));
        }

        [Fact]
        public async Task ChangePasswordWithInvalidUserThrowsException()
        {
            var service = await BuildService(new List<IUser> {GetUserWithVpSales()});
            var result = await service.Login(new LoginRequest() { Username = "sanchari.gb", Password = "test123",Portal = "vpOfSales" });
            //parse the token result
            var token = GetTokenHandler().Parse(result.Token);
            Assert.NotNull(token.Scope);
            Assert.NotNull(Array.Find(token.Scope, element => element.Contains("vpOfSales")));
            await
                Assert.ThrowsAsync<UserNotFoundException>(
                    () => service.ChangePassword("notvalid", "notvalid", "newPassword"));
        }

        [Fact]
        public async Task ChangePasswordWithValidPasswordSuccess()
        {
            var service = await BuildService(new List<IUser> { GetUserWithVpSales() });
            //to make sure login works with old password
            await service.Login(new LoginRequest() { Username = "sanchari.gb", Password = "test123", Portal = "vpOfSales" });
            //Change Password
            await service.ChangePassword("sanchari.gb", "test123", "newPassword");
            //Try with old password
            await Assert.ThrowsAsync<InvalidUserOrPasswordException>(() => service.Login(new LoginRequest() { Username = "sanchari.gb", Password = "test123", Portal = "vpOfSales" }));
            //try with new password
            var rawToken = await service.Login(new LoginRequest() { Username = "sanchari.gb", Password = "newPassword", Portal = "vpOfSales" });
            //parse the token result
            var token = GetTokenHandler().Parse(rawToken.Token);
            Assert.NotNull(token.Scope);
            Assert.NotNull(Array.Find(token.Scope, element => element.Contains("vpOfSales")));

        }

        [Fact]
        public async Task ChangePasswordWithInvalidArgumentThrowsException()
        {
            var service = await BuildService(new List<IUser> { GetUserWithVpSales() });

            await
                Assert.ThrowsAsync<ArgumentNullException>(
                    () => service.ChangePassword(null, "test123", "newPassword"));

            await Assert.ThrowsAsync<ArgumentNullException>(() => service.ChangePassword("sanchari.gb", null, "newPassword"));
            await Assert.ThrowsAsync<ArgumentNullException>(() => service.ChangePassword("sanchari.gb", "test", null));

        }

        [Fact]
        public async Task GetChildrenRolesWithInvalidArgumentThrowsException()
        {
            var service = await BuildService(new List<IUser> { GetUserWithVpSales() });
            await Assert.ThrowsAsync<ArgumentException>(() => service.GetChildrenRoles(null));
        }

        [Fact]
        public async Task GetChildrenRolesSuccess()
        {
            var service = await BuildService(new List<IUser> { GetUserWithVpSales() });
            var result = await service.GetChildrenRoles(new List<string> { "VP Of Sales", "Marketing" });
            Assert.NotNull(result);
            Assert.Contains("Loan Coordinator Manager", result);
            Assert.Contains("Loan Coordinator", result);
            Assert.Contains("Sr. Relationship Manager", result);
            Assert.Contains("Relationship Manager", result);
            Assert.Contains("Social Media Coordinator", result);
        }

        private static async Task<IIdentityService> BuildService(IEnumerable<IUser> users=null)
        {
            var userRepository = new FakeUserRepository();
            IPreSignupUserRepository preSignupRepository = null; 
            var configuration = GetConfiguration();
            var roleToPermissionConverter=new RoleToPermissionConverter(configuration);
            var tokenHandler = GetTokenHandler();
            var mockTenant = new Mock<ITenantService>();
            mockTenant.Setup(c => c.Current).Returns(new TenantInfo() {Id = "my-tenant"});

            var mockRedirectRepository = new Mock<IUserRedirectRepository>();
            var mockTenantTime = new Mock<ITenantTime>();
            var service= new IdentityService(
                configuration, userRepository, mockRedirectRepository.Object, preSignupRepository, new Pbkdf2Crypt(),
                new FakeSessionManager(),
                Mock.Of<IEventHubClient>(), tokenHandler, Mock.Of<IEmailService>(),
                Mock.Of<IConfigurationServiceFactory>(), Mock.Of<ITokenReader>(), mockTenant.Object, roleToPermissionConverter,
                Mock.Of<IPasswordComplexityRules>(r => r.Complies(It.IsAny<string>()) == true),
                Mock.Of<IUserNameValidationRule>(r => r.Complies(It.IsAny<string>()) == true),
                mockTenantTime.Object);
            
            if (users != null)
            {
                foreach (var user in users)
                {
                    await service.CreateUser(user);
                }
            }

            return service;
        }

        private static ITokenHandler GetTokenHandler()
        {
            return new TokenHandler(new SecurityTokenConfig(), Mock.Of<ITokenReader>(),
                Mock.Of<ITokenWriter>());
        }

        private static IUser GetUserWithVpSales()
        {
            return new LendFoundry.Security.Identity.User
            {
                Email = "sanchari.gb@sigmainfo.net",
                Name = "Sanchari Bairi",
                Password = "test123",
                PasswordSalt = "test",
                Username = "sanchari.gb",
                Roles = new[] { "VP of Sales" }
            };
        }

        private static Configuration GetConfiguration()
        {
            const string configurationJson = "{											     " +
                                             "	\"sessionTimeout\": 10,												 " +
                                             "	\"tokenTimeout\": 60,                                                " +
                                             "	\"roles\": {                                                         " +
                                             "		\"VP Of Sales\": {                                               " +
                                             "			\"permissions\": [\"vpOfSales\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Loan Coordinator Manager\": {                          " +
                                             "					\"permissions\": [\"LoanCoordinatorManager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Loan Coordinator\": {                          " +
                                             "							\"permissions\": [\"loancordinator\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Sr. Relationship Manager\": {                          " +
                                             "					\"permissions\": [\"srrelationshipmanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Relationship Manager\": {                      " +
                                             "							\"permissions\": [\"relationshipmanager\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"VP Of Operations\": {                                          " +
                                             "			\"permissions\": [\"vpofoperations\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Sr. Loan Processor\": {                                " +
                                             "					\"permissions\": [\"srloanprocessor\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Loan Processor\": {                            " +
                                             "							\"permissions\": [\"loanprocessor\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Credit Specialist Manager\": {                         " +
                                             "					\"permissions\": [\"creditspecialistmanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Credit Specialist\": {                         " +
                                             "							\"permissions\": [\"creditspecialist\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Merchant Underwriter Manager\": {                      " +
                                             "					\"permissions\": [\"merchantunderwritermanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Merchant Underwriter\": {                      " +
                                             "							\"permissions\": [\"merchantunderwriter\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Customer Service Manager\": {                          " +
                                             "					\"permissions\": [\"customerservicemanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Customer Service Representative\": {           " +
                                             "							\"permissions\": [\"customerservicerepresentative\",\"customerrepresentative1\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"QA Manager\": {                                        " +
                                             "					\"permissions\": [\"qamanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"QA\": {                                        " +
                                             "							\"permissions\": [\"qa\",\"q2\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"Director Of Merchant Support\": {                              " +
                                             "			\"permissions\": [\"{*any}\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Merchant Support Specialist\": {                       " +
                                             "					\"permissions\": [\"{*any}\"]                        " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"Marketing\": {                                                 " +
                                             "			\"permissions\": [\"{*any}\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Social Media Coordinator\": {                          " +
                                             "					\"permissions\": [\"{*any}\"]                        " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		}                                                                " +
                                             "	}                                                                    " +
                                             "}                                                                       ";
            var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Configuration>(configurationJson);
            if (configuration.Roles == null || !configuration.Roles.Any())
                throw new InvalidOperationException("Roles are required");
            return configuration;
        }

        [Fact]
        public void ItShouldReturnVpOfSalesAsParent()
        {
            var service=BuildService().Result;
            var parentRoles = service.GetParentRoles("Loan Coordinator Manager").Result.ToList();
            Assert.Equal(1,parentRoles.Count());
            Assert.Equal("VP Of Sales", parentRoles.First());

        }
        [Fact]
        public void ItShouldReturnVpOfOperationsAsParent()
        {
            var service = BuildService().Result;
            var parentRoles = service.GetParentRoles("Sr. Loan Processor").Result.ToList();
            Assert.Equal(1, parentRoles.Count());
            Assert.Equal("VP Of Operations", parentRoles.First());

        }
        [Fact]
        public void ItShouldReturnDirectorOfMerchantSupportAsParent()
        {
            var service = BuildService().Result;
            var parentRoles = service.GetParentRoles("Merchant Support Specialist").Result.ToList();
            Assert.Equal(1, parentRoles.Count());
            Assert.Equal("Director Of Merchant Support", parentRoles.First());

        }

        [Fact]
        public void ItShouldReturnAllParent()
        {
            var service = BuildService().Result;
            var parentRoles = service.GetParentRoles("Relationship Manager").Result.ToList();
            Assert.Equal(2, parentRoles.Count());
            Assert.Equal("Sr. Relationship Manager", parentRoles[1]);
            Assert.Equal("VP Of Sales", parentRoles[0]);
        }

        [Fact]
        public void ItShouldReturnAllParentForQa()
        {
            var service = BuildService().Result;
            var parentRoles = service.GetParentRoles("QA").Result.ToList();
            Assert.Equal(2, parentRoles.Count());
            Assert.Equal("QA Manager", parentRoles[1]);
            Assert.Equal("VP Of Operations", parentRoles[0]);
        }
        [Fact]
        public void ItShouldReturnAllParentForLoanCoordinator()
        {
            var service = BuildService().Result;
            var parentRoles = service.GetParentRoles("Loan Coordinator").Result.ToList();
            Assert.Equal(2, parentRoles.Count());
            Assert.Equal("Loan Coordinator Manager", parentRoles[1]);
            Assert.Equal("VP Of Sales", parentRoles[0]);
        }

        [Fact]
        public void ItShouldReturnEmptyForVpOfOperations()
        {
            var service = BuildService().Result;
            var parentRoles = service.GetParentRoles("VP Of Operations").Result.ToList();
            Assert.Equal(0, parentRoles.Count());
        }


    }
}