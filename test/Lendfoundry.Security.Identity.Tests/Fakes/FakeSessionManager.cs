﻿using LendFoundry.Security.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Security.Tokens;

namespace Lendfoundry.Security.Identity.Tests.Fakes
{
    public class FakeSessionManager : ISessionManager
    {
        private List<string> InMemoryTokens  {get;} = new List<string>();

        public Task<string> Create(IToken token)
        {
            return Task.Run(() =>
            {
                InMemoryTokens.Add(token.Value);
                return token.Value;
            });
        }

        public Task<bool> IsValid(string token)
        {
            return Task.Run(() => InMemoryTokens.Contains(token));
        }

        public Task<bool> Expire(string token)
        {
            return Task.Run(() => InMemoryTokens.Remove(token));
        }
    }
}
