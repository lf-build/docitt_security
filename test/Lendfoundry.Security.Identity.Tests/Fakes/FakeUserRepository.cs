﻿using LendFoundry.Security.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Lendfoundry.Security.Identity.Tests.Fakes
{
    public class FakeUserRepository : IUserRepository
    {
        public List<IUser> InMemoryData { get; set; } = new List<IUser>();
        public async Task Add(IUser user)
        {
            InMemoryData.Add(user);
        }

        public Task<ICollection<IUser>> All()
        {
            throw new NotImplementedException();
        }

        public Task<IUser> Get(string username)
        {
            return Task.Run(() =>
            {
                return InMemoryData
                    .AsQueryable().FirstOrDefault(x => x.Username == username);
            });
        }

        public void Update(IUser user)
        {
            throw new NotImplementedException();
        }

        public void Remove(IUserInfo user)
        {
            throw new NotImplementedException();
        }

        public Task ChangePassword(IUserInfo user, string password)
        {
            return Task.Run(() =>
            {
                var existingUser= InMemoryData
                    .AsQueryable().FirstOrDefault(x => x.Username == user.Username);
                if (existingUser != null) existingUser.Password = password;
                else throw new UserNotFoundException("User Not found");
            });
        }

        public void SetResetToken(IUser user, string token, DateTime expiration)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<IUser>> AllWithRole(string role)
        {
            throw new NotImplementedException();
        }

        public Task ChangeName(IUserInfo user, string name)
        {
            return Task.Run(() =>
            {
                var existingUser = InMemoryData
                    .AsQueryable().FirstOrDefault(x => x.Username == user.Username);
                if (existingUser != null) existingUser.Name = name;
                else throw new UserNotFoundException("User Not found");
            });
        }

        public void UpdateFailedLoginAttempts(IUser user)
        {
        }

        public Task UserActivation(string name, bool IsActive)
        {
            return Task.Run(() =>
            {
                var existingUser = InMemoryData
                    .AsQueryable().FirstOrDefault(x => x.Username == name);
                if (existingUser != null) existingUser.IsActive = IsActive;
                else throw new UserNotFoundException("User Not found");
            });
        }

        public Task UpdateLastLogin(string name, DateTimeOffset date)
        {
            return Task.Run(() =>
            {
                var existingUser = InMemoryData
                    .AsQueryable().FirstOrDefault(x => x.Username == name);
                if (existingUser != null) existingUser.LastLoggedIn = date;
                else throw new UserNotFoundException("User Not found");
            });
        }
    }
}
