# Service Name : docitt_security

- The purpose of security service is to store security of the user and storing user information.

## Getting Started

- Require environment setup.
- Repository and MongoDB access.
- Should have knowledge of Bit-bucket and DotNet CLI(Command-line interface ) commands. 
- Helpful links to start work :
	* https://docs.microsoft.com/en-us/dotnet/core/
	* https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x
		
### Prerequisites

 - Windows, macOS or Linux Operating System.
 - .NET Core SDK (Software Development Kit).
 - .NET Core Command-line interface (CLI) or Visual Studio 2017.
 - MongoDB Management Tool.
 
### Installing

 - Clone source using below git command or source tree

	    git clone <repository_path>
	And go to respective branch for ex develop

		git fetch && git checkout develop	

 - Open command prompt and goto project directory.

 - Restore nuget packages by command

 		dotnet restore -s <Nuget Package Source> <solution_file>

		Ex. dotnet restore -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc Docitt.Security.2017.sln 

 - Build solution by command : 

		dotnet build --no-restore <solution_file>

		Ex. dotnet build --no-restore Docitt.Security.2017.sln
		
 - Run Solution by command
 
		dotnet run --no-build --project <project path>|
		eg. dotnet run --no-build --project src\LendFoundry.Security.Identity.Api\LendFoundry.Security.Identity.Api.csproj
		
## Running the tests

- We have created shellscript file(cover.sh) for running tests and code coverage, which are shared on lendfoundry documents.
- To start the test
	* open cmd 
	* goto project directory
	* type command cover.sh and enter
- you can see all tests results and code coverate report with covered number of lines and percentage on cmd.
- Once testing completed successfully from above command code coverage report html file is geneated on path :
 artifacts\coverage\report\coverage_report\index.html 

### What these tests test and why

- We are using xUnit testing and implementing for below projects.
	* Api
	* Client
	* Service
- xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.
- This test includes all the scenarios which can be happen on realistic usage of this service.
- Help URL : https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test

## Service Configuration

- Name :  security-identity .
- Can get configuration by request below URL and token for specific tenant from postman
	* URL : <configuration_service>/security-identity
	* Sample configurations :
	```
	{
    "sessionTimeout": "480",
    "emailProviderSettings": {
        "SendGrid": {
            "ApiKey": "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",
            "FromName": "Docitt Development",
            "FromAddress": "suresh.b@sigmainfo.net"
        }
    },
    "database": "identity",
    "roles": {
        "Back-Office": {
            "permissions": [
                "{*any}",
                "back-office"
            ],
            "roles": {
                "Loan Officer": {
                    "permissions": [
                        "{*any}",
                        "back-office"
                    ],
                    "roles": {
                        "Loan Officer Assistance": {
                            "permissions": [
                                "{*any}",
                                "back-office"
                            ]
                        }
                    }
                },
                "Processor": {
                    "permissions": [
                        "{*any}",
                        "back-office"
                    ]
                },
                "Underwriter": {
                    "permissions": [
                        "{*any}",
                        "back-office"
                    ]
                },
                "Branch Manager": {
                    "permissions": [
                        "{*any}",
                        "back-office"
                    ]
                },
                "Regional Manager": {
                    "permissions": [
                        "{*any}",
                        "back-office"
                    ]
                },
                "Admin": {
                    "permissions": [
                        "{*any}",
                        "back-office"
                    ]
                },
                "Executive": {
                    "permissions": [
                        "{*any}",
                        "back-office"
                    ]
                }
            }
        },
        "Affinity-Partner": {
            "permissions": [
                "{*any}",
                "affinity-partner"
            ],
            "roles": {
                "Affinity Partner Admin": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Affinity Partner Manager": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Affinity Partner Agent": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Affinity Partner Assistant": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Affinity Partner Coordinator": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Affinity Partner Other": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Realtors": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Builders": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Accountants": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Brokers": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Insurance Agents": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                },
                "Selling Agent": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ],
                    "roles": {
                        "Assistant Selling Agent": {
                            "permissions": [
                                "{*any}",
                                "affinity-partner"
                            ]
                        }
                    }
                },
                "Buying Agent": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ],
                    "roles": {
                        "Assistant Buying Agent": {
                            "permissions": [
                                "{*any}",
                                "affinity-partner"
                            ]
                        }
                    }
                },
                "Escrow Officer": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ],
                    "roles": {
                        "Assistant Escrow Officer": {
                            "permissions": [
                                "{*any}",
                                "affinity-partner"
                            ]
                        }
                    }
                },
                "Title Agent": {
                    "permissions": [
                        "{*any}",
                        "affinity-partner"
                    ]
                }
            }
        },
        "Borrower": {
            "permissions": [
                "{*any}",
                "borrower"
            ],
            "roles": {
                "Co-Borrower": {
                    "permissions": [
                        "{*any}",
                        "borrower"
                    ]
                }
            }
        }
    },
    "emailProvider": "Sendgrid",
    "tokenTimeout": "60",
    "redirectionTokenTimeout": "1",
    "resetPasswordTemplate": "PasswordReset",
    "resetPasswordTemplateVersion": "1.1",
    "accountVerificationTemplate": "AccountVerification",
    "accountVerificationTemplateVersion": "1.1",
    "maxFailedLoginAttempts": 4,
    "accountLockoutPeriod": 10,
    "maxFailedLoginAttemptsErrorMessage": "Your account will get locked after {0} invalid attempts",
    "accountLockoutMessage": "Your account is locked please try after {0} minutes"
}
	```


## Service Rules

NA

## Service API Documentation

- Anyone can access Api end points by entering below URL in any browser whenever service is running locally

		http://localhost:5000/swagger/

## Databases owned by security service

- Database : MongoDB
- Collections : identity

## External Services called

- Sendgrid : Version= [9.9.0] 
	* This library is used for store user details functionality.	

## Environment Variables used by security-identity service

- Envirounment variables are application variables which are used for setting application behaviour at runtime. Ex. We can use different envirounment variables for different servers like DEV, QA etc.
- In lendfoundry any single service is dependent or uses many other lendfoundry services, So for dynamically target different services we are setting envirounment variable in launchSettings.json.
- Below are the envirounment variables in which we are setting only
important services URL's which are used in mostly all lendfoundry services and we are setting it in "src\LendFoundry.Email.Api\Properties\launchSettings.json".
- Other dependent service URL's are taken from configurations.

		"CONFIGURATION_NAME": "security-identity",
		"CONFIGURATION_URL": <configuration service URL>,
		 // ex. "CONFIGURATION_URL": "http://foundation.dev.lendfoundry.com:7001",
		"EVENTHUB_URL": <event hub URL>,
		"NATS_URL": <nats server URL>,
		"TENANT_URL": <tenant server URL>,
		"LOG_LEVEL": "Debug"

## Known Issues and Workarounds

## Changelog