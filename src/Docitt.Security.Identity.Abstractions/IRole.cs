﻿using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public interface IRole : IRoleInfo
    {
        ICollection<IUserInfo> Members { get; }
    }
}