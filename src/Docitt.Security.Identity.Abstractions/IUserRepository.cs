using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IUserRepository
    {
        Task Add(IUser user);

        Task<ICollection<IUser>> All();

        Task<IUser> Get(string username);

        void Update(IUser user);

        void Remove(IUserInfo user);

        Task ChangePassword(IUserInfo user, string password);

        Task ChangePasswordOnly(IUserInfo user, string password);

        void SetResetToken(IUser user, string token, DateTime expiration);

        Task<ICollection<IUser>> AllWithRole(string role);

        Task ChangeName(IUserInfo user, string name);

        void UpdateFailedLoginAttempts(IUser user);

        Task UserActivation(string name, bool IsActive);

        Task UpdateLastLogin(string name, DateTimeOffset date);

        Task MapToParentUser(string userName,string parentUserName);

        Task<ICollection<IUser>> GetAllChildUser(string userName);
        
        Task<ICollection<IUser>> GetAllChildUserWithRole(string userName,string role);

        Task<IUser> GetByToken(string token);

        Task<IUser> GetBySignInToken(string token);
        Task ExpireSignInToken(IUser user);
    }
}