using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IUserRedirectRepository
    {
        Task<IUserRedirect> Get(string username = "", string userId = null);
        Task<IUserRedirect> GetByRedirectionToken(string redirectionToken);
        Task Add(IUserRedirect user);
        void Update(IUserRedirect user);
    }
}