﻿using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public class Role : IRole
    {
        public Role()
        {
            Members = new List<IUserInfo>();
        }

        public string Name { get; set; }

        public ICollection<IUserInfo> Members { get; }
    }
}