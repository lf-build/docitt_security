﻿namespace LendFoundry.Security.Identity
{
    public class NameChangeRequest
    {
        public string Name { get; set; }
    }
}