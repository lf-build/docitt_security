using System;

namespace LendFoundry.Security.Identity
{
    public class RedirectTokenInValidException : Exception
    {
        public RedirectTokenInValidException(string redirectionToken)
        {
            RedirectionToken = redirectionToken;
        }

        public string RedirectionToken { get; set; }
    }

    public class RedirectTokenTimeoutException : Exception
    {
        public RedirectTokenTimeoutException(string redirectionToken)
        {
            RedirectionToken = redirectionToken;
        }

        public string RedirectionToken { get; set; }
    }

    public class InvalidPortalException : Exception
    {
        public InvalidPortalException(string portal)
        {
            Portal = portal;
        }

        public string Portal { get; set; }
    }
}