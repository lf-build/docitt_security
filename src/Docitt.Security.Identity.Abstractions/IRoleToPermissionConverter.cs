﻿using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public interface IRoleToPermissionConverter
    {
        IEnumerable<string> ToPermission(List<string> roles);
    }
}