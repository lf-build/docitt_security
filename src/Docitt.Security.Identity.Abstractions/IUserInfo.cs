﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public interface IUserInfo
    {
        string Id { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        string Username { get; set; }
        string[] Roles { get; set; }

        string InvitationReferenceId { get; set; }
        string BrandedSiteReferenceId { get; set; }
        bool IsActive { get; set; }

        DateTimeOffset? CreatedOn { get; set; }

        DateTimeOffset? ModifyOn { get; set; }

        List<string> Permissions { get; set; }

        DateTimeOffset? LastLoggedIn { get; set; }   
        
        DateTimeOffset? PreviousLoggedIn { get; set; }
        string MobileNumber { get; set; }

        DateTimeOffset? LastPasswordModifyOn { get; set; }

        DateTimeOffset? LastActiveDeactiveOn { get; set; }

        string ParentUserName {get; set;}

        string Source {get;set;}

        string DirectSignInToken {get;set;}

        string DirectSignInUrl {get;set;}


    }
}