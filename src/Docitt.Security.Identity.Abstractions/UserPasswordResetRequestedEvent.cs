namespace LendFoundry.Security.Identity
{
    public class UserPasswordResetRequestedEvent : UserEvent
    {
        public UserPasswordResetRequestedEvent(string eventName,string name,string email,string resetUrl,string username,string role,string dt, string token) : 
        base(eventName,name,email,resetUrl,username,role,dt)
        {
            Token = token;
        }

    }
}