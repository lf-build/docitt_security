namespace LendFoundry.Security.Identity
{
    public interface IResponsePasswordResetRequest
    {
        string Username { get; set; }
        string Email { get; set; }
        string ResetDate { get; set; }
        string ResetToken { get; set; }
        string ResetUrl {get; set;}
        string ResetTokenExpiry {get; set;}
    }
}