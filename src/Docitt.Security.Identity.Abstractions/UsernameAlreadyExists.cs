namespace LendFoundry.Security.Identity
{
    public class UsernameAlreadyExists : InvalidUserOrPasswordException
    {
        public UsernameAlreadyExists(string username) : base(username)
        {
        }
    }
}