﻿namespace LendFoundry.Security.Identity.Models
{
    public class UpdateUserInformation
    {
        public string refid { get; set; }
        public string phoneNumber { get; set; }
        public string countryCode { get; set; }
    }
}
