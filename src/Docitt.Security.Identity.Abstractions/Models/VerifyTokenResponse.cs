﻿namespace LendFoundry.Security.Identity.Models
{
    public class VerifyTokenResponse
    {
        public string refid { get; set; }
        public bool verified { get; set; }
        public UserDetail userDetail { get; set; }
        public ILoginResponse loginResponse { get; set; }
        public bool EmailVerificationRequired { get; set; }
        public bool MobileVerificationRequired { get; set; }
    }
}
