﻿namespace LendFoundry.Security.Identity.Models
{
    public class UpdateUserInformationResponse
    {
        public string refid { get; set; }
        public bool success { get; set; }

        public IUserInfo UserInfo { get; set; }

        public ILoginResponse loginResponse { get; set; }
    }
}
