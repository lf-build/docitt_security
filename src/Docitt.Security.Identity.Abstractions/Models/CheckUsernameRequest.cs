namespace LendFoundry.Security.Identity.Models
{
    public class CheckUsernameRequest
    {
        public string Username { get; set; }
    }
}