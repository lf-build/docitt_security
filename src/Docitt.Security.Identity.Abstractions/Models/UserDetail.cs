﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity.Models
{
    public class UserDetail : IUserInfo
    {
        public UserDetail(IUserInfo user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            Id = user.Id;
            Email = user.Email;
            Name = user.Name;
            Username = user.Username;
            Roles = user.Roles;
            InvitationReferenceId = user.InvitationReferenceId;
            BrandedSiteReferenceId = user.BrandedSiteReferenceId;
            IsActive = user.IsActive;
            Permissions = user.Permissions;
            CreatedOn = user.CreatedOn;
            ModifyOn = user.ModifyOn;
            LastLoggedIn = user.LastLoggedIn;
            LastActiveDeactiveOn = user.LastActiveDeactiveOn;
            PreviousLoggedIn = user.PreviousLoggedIn;
            MobileNumber = user.MobileNumber;
            LastPasswordModifyOn = user.LastPasswordModifyOn;
            ParentUserName = user.ParentUserName;
            Source = user.Source;
            DirectSignInToken = user.DirectSignInToken;
            DirectSignInUrl = user.DirectSignInUrl;
        }

        public string Email { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string[] Roles { get; set; }

        public string Username { get; set; }

        public string InvitationReferenceId { get; set; }

        public string BrandedSiteReferenceId { get; set; }

        public Dictionary<string, object> settings { get; set; }

        public bool IsActive { get; set; }
        public DateTimeOffset? LastActiveDeactiveOn { get; set; }

       public DateTimeOffset? CreatedOn { get; set; }

       public DateTimeOffset? ModifyOn { get; set; }

        public List<string> Permissions { get; set; }

       public DateTimeOffset? LastLoggedIn { get; set; }

       public string MobileNumber { get; set; }

        public DateTimeOffset? LastPasswordModifyOn { get; set; }

        public DateTimeOffset? PreviousLoggedIn { get; set; }

        public string ParentUserName{get; set;}
        public string Source {get;set;}

        public string DirectSignInToken {get;set;}

       public string DirectSignInUrl {get;set;}
    }
}
