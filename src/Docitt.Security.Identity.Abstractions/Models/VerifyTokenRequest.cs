﻿namespace LendFoundry.Security.Identity.Models
{
    public class VerifyTokenRequest
    {
        public string refid { get; set; }
        public string token { get; set; }
    }
}
