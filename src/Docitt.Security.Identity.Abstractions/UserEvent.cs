namespace LendFoundry.Security.Identity
{
    public class UserEvent
    {
        public UserEvent(string username)
        {
            Username = username;
        }

        public UserEvent(string eventname, string name, string email, string resetUrl, 
            string username, string role, string dt)
        {
            EventName = eventname;
            Name = name;
            Email = email;
            ResetUrl = resetUrl;
            Username = username;
            Role = role;
            UTCDate = dt;
        }

        public UserEvent(string eventname, string name, string email, string resetUrl, 
            string username, string role, string dt, string token)
        {
            EventName = eventname;
            Name = name;
            Email = email;
            ResetUrl = resetUrl;
            Username = username;
            Role = role;
            UTCDate = dt;
            Token = token;
        }

        public string EventName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ResetUrl { get; set; }
        public string Username { get; set; }
        public string Token { get; set; } // New request from NAF
        public string Role { get; set; }
        public string UTCDate { get; set; }
    }
}