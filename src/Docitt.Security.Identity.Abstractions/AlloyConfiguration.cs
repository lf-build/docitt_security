﻿using LendFoundry.Security.Identity;

namespace LendFoundry.Security.Identity
{
    public class AlloyConfiguration
    {
        public string Login { get; set; }

        public CaseInsensitiveDictionary<string> SecurityResetUrls { get; set; }

        public CaseInsensitiveDictionary<string> AccountVerificationUrls { get; set; }

        public bool EmailVerificationRequired { get; set; }
        public bool MobileVerificationRequired { get; set; }

        public CaseInsensitiveDictionary<string> SecurityResetWelComeUrls { get; set; }
    }
}
