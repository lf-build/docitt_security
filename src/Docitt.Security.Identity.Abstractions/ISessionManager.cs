using System.Threading.Tasks;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Security.Identity
{
    public interface ISessionManager
    {
        Task<string> Create(IToken token);

        Task<bool> IsValid(string token);

        Task<bool> Expire(string token);
    }
}