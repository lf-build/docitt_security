﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public interface IUser : IUserInfo
    {
        //[System.Runtime.Serialization.IgnoreDataMember]
        string Password { get; set; }
        //[System.Runtime.Serialization.IgnoreDataMember]
        string PasswordSalt { get; set; }
        string ResetToken { get; set; }
        DateTime? ResetTokenExpiration { get; set; }
        int FailedLoginAttempts { get; set; }
        DateTimeOffset? LastFailedLoginAttempt { get; set; }

        string EmailVerificationToken { get; set; }

        DateTime? EmailVerificationTokenExpiration { get; set; }

        bool EmailVerified { get; set; }

        bool PhoneVerified { get; set; }

        string CountryCode { get; set; }
        string Portal { get; set; }

        DateTime? DirectSignInTokenExpiration { get; set; }
        
    }
}
