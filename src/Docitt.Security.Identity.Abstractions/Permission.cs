﻿using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public class Permission 
    {
        public List<string> Permissions { get; set; }

        public CaseInsensitiveDictionary<Permission> Roles { get; set; }
    }
}
