﻿using System;

namespace LendFoundry.Security.Identity
{
    public class UserRedirect : IUserRedirect
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string RedirectionUrl { get; set; }
        public string RedirectionToken { get; set; }
        public DateTimeOffset? RedirectionTokenCreatedTime { get; set; }
        public DateTimeOffset? RedirectionTokenUpdatedTime { get; set; }
        public DateTimeOffset? RedirectionTokenExpirationTime { get; set; }
        public bool RedirectionTokenIsValid { get; set; }
    }
}
