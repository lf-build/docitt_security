namespace LendFoundry.Security.Identity
{
    public class LoginRequest : ILoginRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
        public string Portal { get; set; }
    }
}