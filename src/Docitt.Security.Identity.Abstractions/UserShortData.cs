using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public class UserShortData : IUserShortData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
    }
}