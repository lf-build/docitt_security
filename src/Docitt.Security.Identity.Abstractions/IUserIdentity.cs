﻿using LendFoundry.Foundation.Date;

namespace LendFoundry.Security.Identity
{
    public interface IUserIdentity
    {
        string UserId { get; }
        string Token { get; }
        TimeBucket TokenExpiration { get; }
    }
}
