﻿using LendFoundry.Security.Identity.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IIdentityService
    {
        Task<bool> IsUsernameAvailable(string username);

        Task<VerifyTokenResponse> CreateUser(IUser user);

        Task<IEnumerable<IUserInfo>> GetAllUsers();

        Task<IUser> GetUser(string username);

        Task<IUserInfo> UpdateUser(IUser user);

        Task RemoveUser(string username);

        Task ChangePassword(string username, string currentPassword, string newPassword);

        void ValidateRequestByLOForPasswordReset(IRequestPasswordResetRequest requestPasswordResetRequest,string[] scope);
        
        Task<IResponsePasswordResetRequest> RequestPasswordReset(IRequestPasswordResetRequest requestPasswordResetRequest, bool sendEmail = true);

        Task ResetPassword(string username, string token, string password);

        Task<ILoginResponse> Login(ILoginRequest loginRequest);

        Task<bool> IsAuthenticated(string token);

        Task<bool> IsAuthorized(string token, string uri);

        Task Logout(string token);

        Task<IEnumerable<string>> GetAllRolesFromCurrentUser();

        Task<IEnumerable<string>> GetUserRoles(string username);

        Task<IEnumerable<string>> GetAllRoles();

        Task<IEnumerable<string>> GetChildRoles(string role);

        Task<IEnumerable<string>> GetChildrenRoles(IEnumerable<string> role);

        Task<ICollection<IUser>> GetRoleMembers(string role);

        Task<ICollection<IUserShortData>> GetRoleMembersShortData(string role);

        Task<IUser> GetCurrentUser();
        Task<IEnumerable<string>> GetParentRoles(string role);
        Task ChangeName(string username, string name);

        Task<VerifyTokenResponse> VerifyToken(string refid, string token);

        Task<VerifyTokenResponse> UpdateUserInformation(string refid, string phoneNumber, string countryCode);
        //Task<bool> UpdateSetting(string key, string value);

        Task<bool> ActivateUser(string userName);

        Task<bool> DeActivateUser(string userName);

        Task<IEnumerable<string>> UpdateUserRoles(string username,string[] roles);

        Task<string> GetRedirectionToken(UserRedirectRequest userRedirectRequest);

        Task<UserRedirectResponse> ValidateRedirectionToken(string redirectionToken);

        Task<string> GetTokenUsingLOCredentials(string username);

        Task<bool> MapToParentUser(string username , string parentusername);

         Task<ICollection<IUserInfo>> GetAllChildUser(string username);

        Task<IUserInfo> GetParentUser(string username);

        Task<ICollection<IUserInfo>> GetAllParentUser(string username);

        Task<ICollection<IUserInfo>> GetAllChildUserWithRole(string username,string role);

        Task LockUser(string token);

        Task<ILoginResponse> LoginWithSignInToken(string source,string signInToken);
    }
}