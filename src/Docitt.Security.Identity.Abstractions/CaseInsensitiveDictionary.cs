using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public class CaseInsensitiveDictionary<T> : Dictionary<string,T>
    {
        public CaseInsensitiveDictionary(): base(StringComparer.CurrentCultureIgnoreCase)
        {
            
        }
    }
}