using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public interface IUserShortData
    {
        string Id { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        string Username { get; set; }
    }
}