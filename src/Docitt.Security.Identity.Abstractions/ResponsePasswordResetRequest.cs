namespace LendFoundry.Security.Identity
{
    public class ResponsePasswordResetRequest : IResponsePasswordResetRequest
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string ResetDate { get; set; }
        public string ResetToken { get; set; }
        public string ResetUrl {get; set;}
        public string ResetTokenExpiry {get; set;}
    }
}