﻿using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IPreSignupUserRepository
    {
        Task Add(IUser user);
        void Update(IUser user);

        Task<IUser> Get(string username);
         Task<IUser> GetById(string id);
    }
}