namespace LendFoundry.Security.Identity
{
    public class InvalidUserOrPasswordException : UserNotFoundException
    {
        public InvalidUserOrPasswordException(string username) : base(username)
        {

        }
        public InvalidUserOrPasswordException(string username,string message) : base(username , message)
        {
        }
        
    }
}