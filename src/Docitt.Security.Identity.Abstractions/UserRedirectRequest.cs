namespace LendFoundry.Security.Identity
{
    public class UserRedirectRequest : ILoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Portal { get; set; }
        public string RedirectUrl { get; set; }
    }
}