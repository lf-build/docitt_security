﻿using System;

namespace LendFoundry.Security.Identity
{
    public interface IUserRedirect
    {
        string Id { get; set; }
        string UserId { get; set; }
        string Username { get; set; }
        string RedirectionUrl { get; set; }
        string RedirectionToken { get; set; }
        DateTimeOffset? RedirectionTokenCreatedTime { get; set; }
        DateTimeOffset? RedirectionTokenUpdatedTime { get; set; }
        DateTimeOffset? RedirectionTokenExpirationTime { get; set; }
        bool RedirectionTokenIsValid { get; set; }
    }
}
