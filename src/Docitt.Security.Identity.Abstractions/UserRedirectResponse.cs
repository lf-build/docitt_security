﻿namespace LendFoundry.Security.Identity
{

    public interface IUserRedirectResponse : ILoginResponse
    {
        string Redirecturl { get; set; }
    }
    public class UserRedirectResponse : IUserRedirectResponse
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public string InvitationReferenceId { get; set; }
        public string Redirecturl { get; set; }
    }
}