namespace LendFoundry.Security.Identity
{
    public class UserRedirectionEvent
    {
        public UserRedirectionEvent(string userId, string username, string eventDate)
        {
            UserId = userId;
            Username = username;
            EventDate = eventDate;
        }

        public string UserId { get; set; }

        public string Username { get; set; }
        public string  EventDate { get; set; }
    }
}