﻿using System;

namespace LendFoundry.Security.Identity
{
    public class RequestPasswordResetRequest : IRequestPasswordResetRequest
    {
        public string Portal { get;set; }
        public string Username { get; set; }
        public string RequestSource {get; set;}
    }
}
