﻿namespace LendFoundry.Security.Identity
{
    public class UserSetting
    {
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }
}
