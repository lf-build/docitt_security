using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public class Configuration : IConfiguration
    { 
        public int SessionTimeout { get; set; }
        public int TokenTimeout { get; set; }
        public int RedirectionTokenTimeout { get; set; }
        public CaseInsensitiveDictionary<Permission> Roles { get; set; }

        public string ResetPasswordTemplate { get; set; }
        public string ResetPasswordTemplateVersion { get; set; }
        public string MaxFailedLoginAttemptsErrorMessage { get; set; }
        public int MaxFailedLoginAttempts { get; set; }
        public int AccountLockoutPeriod { get; set; }

        public string AccountLockoutMessage { get; set; }
        public string PasswordComplexityPattern { get; set; }
        public string PasswordComplexityErrorMessage { get; set; }
        public string UserNameComplexityPattern { get; set; }
        public string UserNameComplexityErrorMessage { get; set; }
        public string AccountVerificationTemplate { get; set; }
        public string AccountVerificationTemplateVersion { get; set; }
        
        public string Database { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string ConnectionString { get; set; }
        public string DefaultPasswordLength { get; set; }
        public string RandomGeneratePassword { get; set; }


         public bool SendEmail { get; set; }

        public TemplateConfiguration PasswordResetSuccessTemplate { get; set; }  

        public string[] RequestSources {get;set;}

        public string ResetPasswordAllowedForPortal {get; set;}

        public string ResetPasswordPerformedByPortal {get; set;}

        public CaseInsensitiveDictionary<string> LendingUrlForSources { get; set; }

        public int DirectSignInTokenTimeOut { get; set; }

         public string UserNotFoundErrorMessage {get;set;}
       public  string UserIsNotActiveErrorMessage {get;set;}
        public string TokenExpiredErrorMessage {get;set;}
        public string SourceNotFoundErrorMessage {get;set;}
        public string[] HubTokenGenerationAllowedForRoles {get; set;}
    }
}