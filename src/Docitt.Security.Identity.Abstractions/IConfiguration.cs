using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Security.Identity
{
    public interface IConfiguration : IDependencyConfiguration
    {
        int SessionTimeout { get; set; }
        int TokenTimeout { get; set; }
        int RedirectionTokenTimeout { get; set; }
        CaseInsensitiveDictionary<Permission> Roles { get; set; }
        string ResetPasswordTemplate { get; set; }
        string ResetPasswordTemplateVersion { get; set; }
        string MaxFailedLoginAttemptsErrorMessage { get; set; }
        int MaxFailedLoginAttempts { get; set; }
        int AccountLockoutPeriod { get; set; }

        string AccountLockoutMessage { get; set; }

        string PasswordComplexityPattern { get; set; }
        string PasswordComplexityErrorMessage { get; set; }

        string UserNameComplexityPattern { get; set; }
        string UserNameComplexityErrorMessage { get; set; }

        string AccountVerificationTemplate { get; set; }
        string AccountVerificationTemplateVersion { get; set; }
        string DefaultPasswordLength { get; set; }
        string RandomGeneratePassword { get; set; }

        bool SendEmail { get; set; }
        TemplateConfiguration PasswordResetSuccessTemplate { get; set; } 

        string[] RequestSources {get;set;}

        string ResetPasswordAllowedForPortal {get; set;}

        string ResetPasswordPerformedByPortal {get; set;}

        CaseInsensitiveDictionary<string> LendingUrlForSources { get; set; }

         int DirectSignInTokenTimeOut { get; set; }

         string UserNotFoundErrorMessage {get;set;}
         string UserIsNotActiveErrorMessage {get;set;}
         string TokenExpiredErrorMessage {get;set;}
         string SourceNotFoundErrorMessage {get;set;}

        string[] HubTokenGenerationAllowedForRoles {get; set;}
    }
}