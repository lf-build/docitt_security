﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
#else
using Microsoft.AspNet.Builder;
#endif

namespace LendFoundry.Security.Client
{
    public static class SecurityMiddlewareExtensions
    {
        public static IApplicationBuilder UseSecurityControl(this IApplicationBuilder app)
        {
            return app.UseMiddleware<SecurityControlMiddleware>();
        }
    }
}
