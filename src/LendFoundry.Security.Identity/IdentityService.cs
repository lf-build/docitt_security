
using LendFoundry.Email;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Identity.Models;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Threading.Tasks;
using LendFoundry.Configuration;
using Microsoft.AspNetCore.Routing.Template;
using Microsoft.AspNetCore.Routing;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Security.Identity
{
    public class IdentityService : IIdentityService
    {
        #region "Constructor"

        public IdentityService(
            IConfiguration configuration,
            IUserRepository userRepository,
            IUserRedirectRepository userRedirectRepository,
            IPreSignupUserRepository preSignupRepository,
            ICrypt crypt,
            ISessionManager sessionManager,
            IEventHubClient eventHub,
            ITokenHandler tokenHandler,
            IEmailService emailService,
            IConfigurationServiceFactory configurationServiceFactory,
            ITokenReader tokenReader,
            ITenantService tenant,
            IRoleToPermissionConverter roleToPermissionConverter,
            IPasswordComplexityRules passwordComplexityRules,
            IUserNameValidationRule userNameValidationRule,
            ITenantTime tenantTime,
            ILookupService lookup,
            ILogger logger
        )
        {
            Configuration = configuration;
            UserRepository = userRepository;
            UserRedirectRepository = userRedirectRepository;
            EventHub = eventHub;
            Crypt = crypt;
            SessionManager = sessionManager;
            TokenHandler = tokenHandler;
            EmailService = emailService;
            ConfigurationServiceFactory = configurationServiceFactory;
            TokenReader = tokenReader;
            Tenant = tenant;
            RoleToPermissionConverter = roleToPermissionConverter;
            PasswordComplexityRules = passwordComplexityRules;
            PreSignupRepository = preSignupRepository;
            TenantTime = tenantTime;
            UserNameValidationRule = userNameValidationRule;
            Lookup = lookup ?? throw new ArgumentException($"{nameof(lookup)} is mandatory", nameof(lookup));
            Logger = logger;
        }

        #endregion

        #region "Properties"

        private IConfiguration Configuration { get; }
        private IUserRepository UserRepository { get; }
        private IUserRedirectRepository UserRedirectRepository { get; }
        private IPreSignupUserRepository PreSignupRepository { get; }
        private IEventHubClient EventHub { get; }
        private ICrypt Crypt { get; }
        private ISessionManager SessionManager { get; }
        private ITokenHandler TokenHandler { get; }
        private IEmailService EmailService { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private ITokenReader TokenReader { get; }
        private ITenantService Tenant { get; }
        private IRoleToPermissionConverter RoleToPermissionConverter { get; }
        private IPasswordComplexityRules PasswordComplexityRules { get; }
        private IUserNameValidationRule UserNameValidationRule {get;}
        private ITenantTime TenantTime { get; }

        private Dictionary<string, DateTime> analysis = new Dictionary<string, DateTime>();
        
        private ILookupService Lookup { get; }

        private ILogger Logger {get;}
        
        #endregion

        #region "Private Methods"

        private async Task<ILoginResponse> UserLogin(IUser user, string[] scope)
        {
            analysis.Add("06001UserLogin",TenantTime.Now.DateTime);
            string rawToken = await CreateToken(scope, user.Username.ToLower());
            analysis.Add("06002UpdateLastLoginStart",TenantTime.Now.DateTime);
            await UserRepository.UpdateLastLogin(user.Username, TenantTime.Now);
            analysis.Add("06003UpdateLastLoginFinish",TenantTime.Now.DateTime);
            
            analysis.Add("06004PublishEventUserLoggedIn",TenantTime.Now.DateTime);
            await EventHub.Publish("UserLoggedIn", new UserEvent(user.Username));
            analysis.Add("06005PublishEventUserLoggedInFinished",TenantTime.Now.DateTime);
            return new LoginResponse() { Token = rawToken, UserId = user.Id, InvitationReferenceId= user.InvitationReferenceId };
        }

        private async Task<IUserInfo> AddUser(IUser user)
        {
            try
            {
                var anotherUser = UserRepository.Get(user.Username).Result;
                if (anotherUser != null)
                    throw new UsernameAlreadyExists(user.Username);

                //user.PasswordSalt = Crypt.CreateSalt();
                //user.Password = Crypt.Encrypt(user.Password, user.PasswordSalt);

                user.LastActiveDeactiveOn = TenantTime.Now;

                await UserRepository.Add(user);
                //await EventHub.Publish("UserCreated", new UserEvent(user.Username));

                await EventHub.Publish("UserCreated", new UserEvent(
                   "UserCreated",
                   user.Name,
                   user.Email,
                   string.Empty,
                   user.Username,
                   (user.Roles != null) ? string.Join("", user.Roles) : "",
                   DateTime.UtcNow.ToString("g", System.Globalization.CultureInfo.InvariantCulture)));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return user;
        }

        private AlloyConfiguration GetAlloyConfiguration()
        {
            var serviceName = "alloy";
            var configurationService = ConfigurationServiceFactory.Create<AlloyConfiguration>(serviceName, TokenReader);
            var configuration = configurationService.Get();
            if (configuration == null)
                throw new Exception($"Configuration not found for '{serviceName}'");
            return configuration;
        }

        private void ValidateScopeAccess(string[] scope, string username, string portal)
        {
            if (scope == null) throw new ArgumentNullException(nameof(scope));
            if (portal == null) throw new ArgumentNullException(nameof(portal));

            if (!scope.Contains(portal))
                throw new InvalidUserOrPasswordException(username);
        }

        private async Task<string> CreateToken(string[] scope, string Username)
        {
            var expiration = TenantTime.Now.DateTime.AddMinutes(Configuration.SessionTimeout);
            var token = TokenHandler.Issue(Tenant.Current.Id, "docitt", expiration, Username, scope);

            var rawToken = await SessionManager.Create(token);
            return rawToken;
        }

        private void Authenticate(IUser user, string password)
        {
            if (IsAccountLocked(user))
            {
                throw new AccountLockedException(user.Username, string.Format(Configuration.AccountLockoutMessage, Configuration.AccountLockoutPeriod) ?? $"Invalid login credentials for {user.Username}");
            }

            if (HasReachedMaxFailedLoginAttempts(user) && !IsInLockoutPeriod(user))
                user.FailedLoginAttempts = 0;

            if (user.Password != Crypt.Encrypt(password, user.PasswordSalt))
            {
                RecordFailedLoginAttempt(user);

                //Check if user reached the max failed login attempts.
                if (HasReachedMaxFailedLoginAttempts(user))
                {                    
                    throw new AccountLockedException(user.Username, string.Format(Configuration.AccountLockoutMessage, Configuration.AccountLockoutPeriod) ?? $"Invalid login credentials for {user.Username}");
                }

                int remainingAttempts = Configuration.MaxFailedLoginAttempts - user.FailedLoginAttempts;
                throw new InvalidUserOrPasswordException(user.Username, string.Format(Configuration.MaxFailedLoginAttemptsErrorMessage, remainingAttempts));
            }

            if (user.FailedLoginAttempts > 0)
                ResetFailedLoginAttempts(user);
        }

        private void ResetFailedLoginAttempts(IUser user)
        {
            user.FailedLoginAttempts = 0;
            user.LastFailedLoginAttempt = null;
            UserRepository.UpdateFailedLoginAttempts(user);
        }

        private void RecordFailedLoginAttempt(IUser user)
        {
            user.FailedLoginAttempts++;
            user.LastFailedLoginAttempt = TenantTime.Now;
            UserRepository.UpdateFailedLoginAttempts(user);
        }

        private bool IsAccountLocked(IUser user)
        {
            return HasReachedMaxFailedLoginAttempts(user) && IsInLockoutPeriod(user);
        }

        private bool IsInLockoutPeriod(IUser user)
        {
            if (user.LastFailedLoginAttempt == null)
                return false;

            var lockoutTime = (TenantTime.Now - user.LastFailedLoginAttempt.Value).TotalMinutes;

            return lockoutTime < Configuration.AccountLockoutPeriod;
        }

        private bool HasReachedMaxFailedLoginAttempts(IUser user)
        {
            return user.FailedLoginAttempts >= Configuration.MaxFailedLoginAttempts;
        }

#if DOTNET2

        private static bool HasAccess(IToken token, string uri)
        {
            if (token.Scope == null || !token.Scope.Any())
                return false;

            var baseUri = new Uri("http://lenfoundry.com");
            var request = new Uri($"{baseUri}{uri}");
            return token.Scope.Any(scope =>
            {
                var template = TemplateParser.Parse($"{scope}");
                var matcher = new TemplateMatcher(template, GetDefaults(template));
                var values = new RouteValueDictionary();
                return matcher.TryMatch(request.AbsolutePath, values) &&
                    template.Segments.Count() == values.Keys.Count();
            });
        }

        private static RouteValueDictionary GetDefaults(RouteTemplate parsedTemplate)
        {
            var result = new RouteValueDictionary();
            var parameters = parsedTemplate.Parameters.Where(p => p.DefaultValue != null);

            foreach (var parameter in parameters)
            {
                result.Add(parameter.Name, parameter.DefaultValue);
            }

            return result;
        }

#else
        private static bool HasAccess(IToken token, string uri)
        {
            if (token.Scope == null || !token.Scope.Any())
                return false;

            var baseUri = new Uri("http://lenfoundry.com");
            var request = new Uri($"{baseUri}{uri}");
            return token.Scope.Any(scope =>
            {
                var template = new UriTemplate($"{scope}");
                var match = template.Match(baseUri, request);
                return match != null && template.PathSegmentVariableNames.Count == match.BoundVariables.Count;
            });
        }
#endif

         private string EnsureSourceIsValid(string source)
        {
            source = source.ToLower();
            if (Lookup.GetLookupEntry("sources", source) == null)
                throw new ArgumentException(Configuration.SourceNotFoundErrorMessage ?? "Invalid source");
            return source;
        }
        private void ValidatePassword(string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException(nameof(password));

            if (!PasswordComplexityRules.Complies(password))
                throw new ArgumentException(Configuration.PasswordComplexityErrorMessage);
        }

        private static void ValidateEmail(string email)
        {
            bool isValid = Regex.IsMatch(email,
                @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);

            if (!isValid)
                throw new ArgumentException("Email property doesn't have valid value", nameof(email));
        }

        private static IEnumerable<string> GetRoleNamesFromPermission(KeyValuePair<string, Permission> role)
        {
            var roles = new List<string>();
            roles.Add(role.Key);
            if (role.Value.Roles != null)
                foreach (var childRole in role.Value.Roles)
                    roles.AddRange(GetRoleNamesFromPermission(childRole));
            return roles;
        }

        private KeyValuePair<string, Permission> GetRoleByName(CaseInsensitiveDictionary<Permission> roles, string name)
        {
            foreach (var role in roles)
            {
                if (role.Key.Equals(name))
                    return role;
                if (role.Value.Roles != null)
                {
                    var child = GetRoleByName(role.Value.Roles, name);
                    if (!default(KeyValuePair<string, Permission>).Equals(child))
                        return child;
                }
            }
            return default(KeyValuePair<string, Permission>);
        }

        private Tuple<bool, List<string>> GetParents(CaseInsensitiveDictionary<Permission> roles, string name, string parent = null)
        {
            var parents = new List<string>();
            if (parent != null)
                parents.Add(parent);

            foreach (var role in roles)
            {
                if (role.Key.Equals(name))
                {
                    return new Tuple<bool, List<string>>(true, parents);
                }
                if (role.Value.Roles != null)
                {
                    var result = GetParents(role.Value.Roles, name, role.Key);
                    if (result.Item1)
                    {
                        parents.AddRange(result.Item2);
                        return new Tuple<bool, List<string>>(true, parents);
                    }
                }
            }
            return new Tuple<bool, List<string>>(false, parents);
        }

        #endregion

        #region "Public Methods"

        public async Task<bool> IsUsernameAvailable(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));

            return (await UserRepository.Get(username)) == null;
        }

        public async Task<IUser> GetCurrentUser()
        {
            var token = TokenReader.Read();
            if (await IsAuthenticated(token))
            {
                var userName = TokenHandler.Parse(token).Subject;
                userName = WebUtility.UrlEncode(userName);

                return await GetUser(userName);
            }
            throw new AuthenticationException();
        }

        public async Task<VerifyTokenResponse> CreateUser(IUser user)
        {
            var objResponse = new VerifyTokenResponse();
            bool isValidSource = false;
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(user.Name))
                throw new ArgumentNullException(nameof(user.Name));

            if (string.IsNullOrEmpty(user.Email))
                throw new ArgumentNullException(nameof(user.Email));

            ValidateEmail(user.Email);

            if (string.IsNullOrEmpty(user.Username))
                throw new ArgumentNullException(nameof(user.Username));

            if (string.IsNullOrWhiteSpace(user.Username))
                throw new ArgumentNullException(nameof(user.Username));

            if(!UserNameValidationRule.Complies(user.Username))
                throw new ArgumentException(Configuration.UserNameComplexityErrorMessage);

            //if source is passed then we have to consider this for Banker View signup
            if(!string.IsNullOrEmpty(user.Source))
            {
                EnsureSourceIsValid(user.Source);
                //validate lending page source url is configured or not

                if (Configuration.LendingUrlForSources != null)
                {
                    if (!Configuration.LendingUrlForSources.ContainsKey(user.Source))
                        throw new ArgumentException($"Lending Url For source  '{user.Source}' does not exist in configuration");
                }
                else
            {
                throw new ArgumentException("Lending Url For source does not exist in configuration.");
            }

                isValidSource = true;
            }

            if (!string.IsNullOrEmpty(user.ParentUserName))
            {
                    var parentUser = await UserRepository.Get(user.ParentUserName);
                    if (parentUser == null)
                            throw new UserNotFoundException(user.ParentUserName);
            }
            if(user.Roles == null  || user.Roles.Count() <= 0)
            {
                throw new ArgumentNullException(nameof(user.Roles));
            }
            else
            {
                foreach (var userRole in user.Roles)
                {
                    var roleName = GetRoleByName(Configuration.Roles, userRole);
                    if (string.IsNullOrEmpty(roleName.Key))
                        throw new ArgumentException("'" + userRole + "' role does not exist.");
                }
            }

            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();
            ValidateScopeAccess(scope, user.Username, user.Portal);

            ValidatePassword(user.Password);

            var alloyConfig = GetAlloyConfiguration();
            if (alloyConfig.AccountVerificationUrls != null)
            {
                if (!alloyConfig.AccountVerificationUrls.ContainsKey(user.Portal))
                    throw new ArgumentNullException(nameof(user.Portal));
            }
            else
            {
                throw new ArgumentException("account verification url does not exist.");
            }

            DateTime? emailVerificationTokenExpiry = null;
            string emailVerificationToken = string.Empty;
            if (alloyConfig.EmailVerificationRequired)
            {
                emailVerificationTokenExpiry = TenantTime.Now.DateTime.AddMinutes(Configuration.TokenTimeout);
                emailVerificationToken = $"{Guid.NewGuid():N}{Guid.NewGuid():N}";
                user.EmailVerificationToken = emailVerificationToken;
                user.EmailVerificationTokenExpiration = emailVerificationTokenExpiry;
            }

            user.PasswordSalt = Crypt.CreateSalt();
            user.Password = Crypt.Encrypt(user.Password, user.PasswordSalt);

            user.EmailVerified = false;
            user.PhoneVerified = false;
            user.IsActive = true;
            DateTime? directSignInTokenExpiration = TenantTime.Now.DateTime.AddMinutes(Configuration.DirectSignInTokenTimeOut);
            //Note: we will create token always
            user.DirectSignInToken = $"{Guid.NewGuid():N}{Guid.NewGuid():N}";
            user.DirectSignInTokenExpiration = directSignInTokenExpiration;

            if(isValidSource)
            { //this is for Banker PathView
                user.DirectSignInUrl = string.Format(Configuration.LendingUrlForSources[user.Source],user.Source,user.DirectSignInToken);          

                if(!string.IsNullOrEmpty(user.BrandedSiteReferenceId))
                { 
                    //TODO : Need to find way to for optional parameter in url                  
                    user.DirectSignInUrl = user.DirectSignInUrl + "/reference:" + user.BrandedSiteReferenceId;   
                }
            }
            await PreSignupRepository.Add(user);

            if (alloyConfig.EmailVerificationRequired)
            {
                await EmailService.Send("user",user.Username,Configuration.AccountVerificationTemplate, new
                {
                    user.Email,
                    user.Username,
                    ResetToken = emailVerificationToken,
                    accountVerificationUrl = string.Format(alloyConfig.AccountVerificationUrls[user.Portal], emailVerificationToken, user.Id),
                    Configuration.TokenTimeout
                });
            }
            else if (!alloyConfig.MobileVerificationRequired)
            {
                await AddUser(user);

                //now as per new login need to login user
                var loginResponse = await UserLogin(user, scope);

                objResponse.loginResponse = loginResponse;
            }
            objResponse.userDetail = new UserDetail(user);
            if (alloyConfig != null)
            {
                objResponse.EmailVerificationRequired = alloyConfig.EmailVerificationRequired;
                objResponse.MobileVerificationRequired = alloyConfig.MobileVerificationRequired;
            }
            return objResponse;
        }

        public async Task<VerifyTokenResponse> VerifyToken(string refid, string token)
        {
            var alloyConfig = GetAlloyConfiguration();
            var objResponse = new VerifyTokenResponse();

            if (!alloyConfig.EmailVerificationRequired)
                throw new ArgumentException("email verification configuration not requried.");

            if (string.IsNullOrEmpty(refid))
                throw new ArgumentNullException(nameof(refid));

            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            var user = await PreSignupRepository.GetById(refid);

            if (user == null)
                throw new UserNotFoundException(refid);

            if (user.EmailVerificationToken != token)
                throw new ArgumentException("The verification token is invalid.", nameof(token));

            if (user.EmailVerificationTokenExpiration < TenantTime.Now.DateTime)
                throw new ArgumentException("The verification token is expired.", nameof(token));

            if (user.EmailVerified)
                throw new ArgumentException("The verification token is invalid.", nameof(token));
            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();
            ValidateScopeAccess(scope, user.Username, user.Portal);
            user.EmailVerified = true;
            PreSignupRepository.Update(user);

            if (alloyConfig.EmailVerificationRequired && !alloyConfig.MobileVerificationRequired)
            {
                await AddUser(user);

                var loginResponse = await UserLogin(user, scope);

                objResponse.loginResponse = loginResponse;
            }
            objResponse.verified = true;
            objResponse.userDetail = new UserDetail(user);

            if (alloyConfig != null)
            {
                objResponse.EmailVerificationRequired = alloyConfig.EmailVerificationRequired;
                objResponse.MobileVerificationRequired = alloyConfig.MobileVerificationRequired;
            }
            return objResponse;
        }

        public async Task<VerifyTokenResponse> UpdateUserInformation(string refid, string phoneNumber, string countryCode)
        {
            var alloyConfig = GetAlloyConfiguration();
            var objResponse = new VerifyTokenResponse();
            //   IUserInfoWrapper objUserInfo = null;
            if (string.IsNullOrEmpty(refid))
                throw new ArgumentNullException(nameof(refid));

            var user = await PreSignupRepository.GetById(refid);

            if (user == null)
                throw new UserNotFoundException(refid);

            if (!user.EmailVerified && alloyConfig.EmailVerificationRequired)
                throw new ArgumentException("The email verification is incomplete.");
            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();
            ValidateScopeAccess(scope, user.Username, user.Portal);

            if (!string.IsNullOrEmpty(phoneNumber) && !string.IsNullOrEmpty(countryCode))
            {
                user.PhoneVerified = true;
                user.MobileNumber = phoneNumber;
                user.CountryCode = countryCode;
                PreSignupRepository.Update(user);
            }
            await AddUser(user);

            //as per discussion we have to login user after successfull verification.

            var loginResponse = await UserLogin(user, scope);
            objResponse.loginResponse = loginResponse;
            objResponse.userDetail = new UserDetail(user);
            return objResponse;
        }

        public async Task<IEnumerable<IUserInfo>> GetAllUsers()
        {
            return await UserRepository.All();
        }

        public async Task<IUser> GetUser(string username)
        {
            username = WebUtility.UrlDecode(username);
            username = username.Replace(" ","+");
            
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await UserRepository.Get(username);
            if (user == null)
                throw new UserNotFoundException(username);
            user.Permissions = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToList();

            return user;
        }

        public async Task<IUserInfo> UpdateUser(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            UserRepository.Update(user); 
            await EventHub.Publish("UserUpdated", new UserEvent(user.Username));
            return user;
        }

        public async Task RemoveUser(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await UserRepository.Get(username);

            if (user == null)
                throw new UserNotFoundException(username);

            UserRepository.Remove(user);
            await EventHub.Publish("UserRemoved", new UserEvent(user.Username));
        }

        public async Task ChangePassword(string username, string currentPassword, string newPassword)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            if (string.IsNullOrEmpty(currentPassword))
                throw new ArgumentNullException(nameof(currentPassword));

            ValidatePassword(newPassword);

            var user = await UserRepository.Get(username);

            if (user == null)
                throw new UserNotFoundException(username);

            if (!user.IsActive)
                throw new ArgumentException("User is not active");

            Authenticate(user, currentPassword);

            await UserRepository.ChangePassword(user, Crypt.Encrypt(newPassword, user.PasswordSalt));
            await EventHub.Publish("UserPasswordChanged", new UserEvent(user.Username));
        }

        public void ValidateRequestByLOForPasswordReset(IRequestPasswordResetRequest requestPasswordResetRequest,string[] scope)
        {
            if(!scope.Contains(Configuration.ResetPasswordPerformedByPortal))
                    throw new InvalidUserOrPasswordException(string.Empty);

            if(string.IsNullOrEmpty(requestPasswordResetRequest.Portal))
                    throw new ArgumentException("Invalid data in payload.");

            if(string.IsNullOrEmpty(requestPasswordResetRequest.RequestSource))
                    throw new ArgumentException("Invalid data in payload.");

            if (string.IsNullOrEmpty(requestPasswordResetRequest.Username))
                    throw new Exception("Username is required.");

            if(!requestPasswordResetRequest.Portal.Equals(Configuration.ResetPasswordAllowedForPortal))
                    throw new ArgumentException("Invalid data in paylod.");

            if(!Configuration.RequestSources.Contains(requestPasswordResetRequest.RequestSource))
                    throw new ArgumentException("Invalid data in paylod.");
        }

        public async Task<IResponsePasswordResetRequest> RequestPasswordReset(IRequestPasswordResetRequest requestPasswordResetRequest, bool sendEmail=true)
        {
            if (string.IsNullOrEmpty(requestPasswordResetRequest.Username))
                throw new ArgumentNullException(nameof(requestPasswordResetRequest.Username));

            if (string.IsNullOrEmpty(requestPasswordResetRequest.Portal))
                throw new ArgumentNullException(nameof(requestPasswordResetRequest.Portal));

            var user = await UserRepository.Get(requestPasswordResetRequest.Username);

            if (user == null)
                throw new UserNotFoundException(requestPasswordResetRequest.Username);

            if (!user.IsActive)
                throw new ArgumentException("User is not active");

                 var alloyConfig = GetAlloyConfiguration();
            if (!alloyConfig.SecurityResetUrls.ContainsKey(requestPasswordResetRequest.Portal))
                throw new ArgumentNullException(nameof(requestPasswordResetRequest.Portal));
            
            var resetToken = setResetToken(user);
            var ResetUrl = string.Empty;
            var ResetTokenExpiry = DateTime.UtcNow.AddHours(Configuration.TokenTimeout).ToString("g", System.Globalization.CultureInfo.InvariantCulture);
            var ResetDate = DateTime.UtcNow.ToString("g", System.Globalization.CultureInfo.InvariantCulture);
            
            if(Configuration.RequestSources != null && !string.IsNullOrEmpty(requestPasswordResetRequest.RequestSource) && Configuration.RequestSources.Any(x => x.ToLower().Equals(requestPasswordResetRequest.RequestSource.ToLower())))
            {
                ResetUrl = string.Format(alloyConfig.SecurityResetWelComeUrls[requestPasswordResetRequest.Portal], resetToken);
            }
            else{   
                ResetUrl = string.Format(alloyConfig.SecurityResetUrls[requestPasswordResetRequest.Portal], resetToken);
            }
            
            if (sendEmail)
            {
                await EmailService.Send("user",user.Username, Configuration.ResetPasswordTemplate, new
                    {
                        user.Name,
                        user.Email,
                        user.Username,
                        ResetToken = resetToken,
                        LoginUrl = ResetUrl,
                        Configuration.TokenTimeout
                    });
            }
            
            // Raise Event
            await EventHub.Publish("UserPasswordResetRequested", new UserPasswordResetRequestedEvent(
                 "UserPasswordResetRequested", 
                user.Name,
                user.Email,
                ResetUrl,
                user.Username, 
                string.Empty,
                ResetDate,
                resetToken));

            IResponsePasswordResetRequest response = new ResponsePasswordResetRequest(){
                Username = user.Username,
                Email = user.Email,
                ResetUrl = ResetUrl,
                ResetDate = ResetDate,
                ResetToken = resetToken,
                ResetTokenExpiry = ResetTokenExpiry
            };

            return response;
        }


        private string setResetToken(IUser user)
        {

            var resetToken = $"{Guid.NewGuid():N}{Guid.NewGuid():N}";          

            UserRepository.SetResetToken(user, resetToken, TenantTime.Now.DateTime.AddMinutes(Configuration.TokenTimeout));
            return resetToken;
        }
        public async Task ResetPassword(string username, string token, string password)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            ValidatePassword(password);

            var user = await UserRepository.Get(username);

            if (user == null)
                throw new UserNotFoundException(username);

            if (!user.IsActive)
                throw new ArgumentException("User is not active.");

             if (user.ResetToken != token)
                 throw new ArgumentException("The reset password token is invalid.", nameof(token));

             if (user.ResetTokenExpiration < TenantTime.Now.DateTime)
                 throw new ArgumentException("The reset password token is expired.", nameof(token));

                var alloyConfig = GetAlloyConfiguration();
            if (!alloyConfig.SecurityResetUrls.ContainsKey(user.Portal))
                throw new ArgumentNullException(nameof(user.Portal));
                            
            await UserRepository.ChangePassword(user, Crypt.Encrypt(password, user.PasswordSalt));
            var resetToken = setResetToken(user);
            var ResetDate = DateTime.UtcNow.ToString("g", System.Globalization.CultureInfo.InvariantCulture);
            var ResetUrl = string.Format(alloyConfig.SecurityResetUrls[user.Portal], resetToken) + "/success:true";
             if (Configuration.SendEmail)
            {
                await EmailService.Send("user",user.Username,Configuration.PasswordResetSuccessTemplate.Name, new
                {
                    user.Name,
                    user.Email,
                    ResetDate,
                    ResetUrl,
                });
            }

            await EventHub.Publish("UserPasswordChanged", new UserEvent(
                "UserPasswordChanged", 
                user.Name,
                user.Email,
                ResetUrl,
                user.Username,
                (user.Roles != null) ? string.Join("", user.Roles) : "",
                ResetDate,
                token));
        }

        public async Task<ILoginResponse> Login(ILoginRequest loginRequest)
        {   
            analysis.Add("01LoginServiceStart",TenantTime.Now.DateTime);
            if (string.IsNullOrEmpty(loginRequest.Username))
                throw new ArgumentNullException(nameof(loginRequest.Username));

            if (string.IsNullOrEmpty(loginRequest.Password))
                throw new ArgumentNullException(nameof(loginRequest.Password));

            if (string.IsNullOrEmpty(loginRequest.Portal))
                throw new ArgumentNullException(nameof(loginRequest.Portal));

            analysis.Add("02GetStart",TenantTime.Now.DateTime);
            var user = await UserRepository.Get(loginRequest.Username);
            analysis.Add("03GetFinish",TenantTime.Now.DateTime);

            if (user == null)
                throw new InvalidUserOrPasswordException(loginRequest.Username, "");

            if (!user.IsActive)
                throw new ArgumentException("User is not active.");
            
            analysis.Add("04AuthenticateStart",TenantTime.Now.DateTime);
            Authenticate(user, loginRequest.Password);
            analysis.Add("04AuthenticateFinish",TenantTime.Now.DateTime);
            
            analysis.Add("05ValidateScopeStart",TenantTime.Now.DateTime);
            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();
            ValidateScopeAccess(scope, user.Username, loginRequest.Portal);
            analysis.Add("05ValidateScopeFinished",TenantTime.Now.DateTime);
            
            analysis.Add("06UserLoginStart",TenantTime.Now.DateTime);
            var result = await UserLogin(user, scope);
            analysis.Add("06UserLoginFinish",TenantTime.Now.DateTime);
            analysis.Add("07LoginServiceFinish",TenantTime.Now.DateTime);

            string printlog = string.Empty;
            DateTime previousTime = new DateTime();
            foreach (var kvp in analysis)
            {
                printlog += "\n" + $"{kvp.Key} \tTime:{kvp.Value.ToString("hh.mm.ss.ffffff")} \tDiff:{(kvp.Value-previousTime).TotalMilliseconds}";
                previousTime = kvp.Value;
            }
            
            Logger.Info(printlog);
            return result;
        }

        public async Task<bool> IsAuthenticated(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            return await SessionManager.IsValid(token);
        }

        public async Task<bool> IsAuthorized(string token, string uri)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            return await SessionManager.IsValid(token) && HasAccess(TokenHandler.Parse(token), uri);
        }

        public async Task Logout(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            if (await SessionManager.Expire(token))
            {
                var username = TokenHandler.Parse(token).Subject;
                var user = await UserRepository.Get(username);
                if (user != null)
                    await EventHub.Publish("UserLoggedOut", new UserEvent(user.Username));
            }
        }

        public async Task<IEnumerable<string>> GetAllRolesFromCurrentUser()
        {
            var currentUser = await GetCurrentUser();
            var user = await GetUser(currentUser.Username);
            if (user == null)
                throw new UserNotFoundException(currentUser.Username);
            return user.Roles;
        }

        public async Task<IEnumerable<string>> GetUserRoles(string username)
        {
            var user = await GetUser(username);
            if (user == null)
                throw new UserNotFoundException(username);
            return user.Roles;
        }

        public async Task<IEnumerable<string>> GetAllRoles()
        {
            await Task.Yield();

            var allRoles = new List<string>();
            foreach (var role in Configuration.Roles)
                allRoles.AddRange(GetRoleNamesFromPermission(role));
            return allRoles;
        }

        public async Task<IEnumerable<string>> GetChildRoles(string role)
        {
            await Task.Yield();

            var childRoles = new List<string>();
            var found = GetRoleByName(Configuration.Roles, role);
            foreach (var child in found.Value.Roles)
                childRoles.AddRange(GetRoleNamesFromPermission(child));
            return childRoles;
        }

        public async Task<IEnumerable<string>> GetParentRoles(string role)
        {
            await Task.Yield();

            var result = GetParents(Configuration.Roles, role);
            return result.Item1 ? result.Item2 : new List<string>();
        }

        public async Task<IEnumerable<string>> GetChildrenRoles(IEnumerable<string> roles)
        {
            await Task.Yield();

            if (roles == null)
                throw new ArgumentException("Value cannot be null or empty", nameof(roles));

            var childRoles = new List<string>();
            foreach (var role in roles)
            {
                var found = GetRoleByName(Configuration.Roles, role);
                if (found.Value?.Roles != null)
                {
                    foreach (var child in found.Value.Roles)
                        childRoles.AddRange(GetRoleNamesFromPermission(child));
                }
            }
            return childRoles;
        }

        public async Task<ICollection<IUser>> GetRoleMembers(string role)
        {
            if (string.IsNullOrEmpty(role))
                throw new ArgumentNullException(nameof(role));

            return await UserRepository.AllWithRole(role);
        }

        public async Task<ICollection<IUserShortData>> GetRoleMembersShortData(string role)
        {
            if (string.IsNullOrEmpty(role))
                throw new ArgumentNullException(nameof(role));

            var users = await UserRepository.AllWithRole(role);
            ICollection<IUserShortData> result = new Collection<IUserShortData>();

            foreach(var user in users)
            {
                IUserShortData row = new UserShortData(){
                    Id = user.Id,
                    Username = user.Username,
                    Name = user.Name,
                    Email = user.Email
                };

                result.Add(row);
            }

            return result;
        }

        public async Task ChangeName(string username, string name)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            var user = await UserRepository.Get(username);

            if (user == null)
                throw new UserNotFoundException(username);

            if (!user.IsActive)
                throw new ArgumentException("User is not active.");

            await UserRepository.ChangeName(user, name);
        }

        public async Task<bool> ActivateUser(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("UserName is Required");

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            await UserRepository.UserActivation(userName, true);

            await EventHub.Publish("UserUpdated", new UserEvent(userName));
            return true;
        }

        public async Task<bool> DeActivateUser(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("UserName is Required");

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            await UserRepository.UserActivation(userName, false);
            await EventHub.Publish("UserUpdated", new UserEvent(userName));
            return true;
        }

        public async Task<IEnumerable<string>> UpdateUserRoles(string username, string[] roles)
        {
            if (string.IsNullOrEmpty(username))
                throw new UserNotFoundException($"User {username} cannot be found");

            username = WebUtility.UrlDecode(username);
            username = username.Replace(" ","+");
            
            var user = await GetUser(username);
            if (user == null)
                throw new ArgumentNullException(nameof(username));

            if (roles == null || !roles.Any())
            {
                throw new ArgumentNullException(nameof(roles));
            }
            else
            {
                foreach (var userRole in roles)
                {
                    var roleName = GetRoleByName(Configuration.Roles, userRole);
                    if (string.IsNullOrEmpty(roleName.Key))
                        throw new ArgumentException("'" + userRole + "' role does not exist.");
                }
            }

            user.Roles = roles;
            UserRepository.Update(user);
            await EventHub.Publish("UserUpdated", new UserEvent(user.Username));

            return user.Roles;
        }

        public async Task<string> GetRedirectionToken(UserRedirectRequest userRedirectRequest)
        {
            if (string.IsNullOrEmpty(userRedirectRequest.Username))
                throw new ArgumentNullException(nameof(userRedirectRequest.Username));

            if (string.IsNullOrEmpty(userRedirectRequest.Password))
                throw new ArgumentNullException(nameof(userRedirectRequest.Password));

            if (string.IsNullOrEmpty(userRedirectRequest.Portal))
                throw new ArgumentNullException(nameof(userRedirectRequest.Portal));

            if (!userRedirectRequest.Portal.ToLower().Equals("back-office"))
                throw new InvalidPortalException(nameof(userRedirectRequest.Portal));

            var user = await UserRepository.Get(userRedirectRequest.Username);

            if (user == null)
                throw new InvalidUserOrPasswordException(userRedirectRequest.Username);

            if (!user.IsActive)
                throw new ArgumentException("User is not active.");

            // TODO : Send password in base64 string in request and Decode here (userRedirectRequest.Password)
            if (IsAccountLocked(user))
            {
                Configuration.AccountLockoutMessage = string.Format(Configuration.AccountLockoutMessage, Configuration.AccountLockoutPeriod);
                throw new AccountLockedException(user.Username, Configuration.AccountLockoutMessage ?? $"Invalid login credentials for {user.Username}");
            }

            if (HasReachedMaxFailedLoginAttempts(user) && !IsInLockoutPeriod(user))
                user.FailedLoginAttempts = 0;

            try
            {
                userRedirectRequest.Password = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(userRedirectRequest.Password));
            }
            catch (FormatException)
            {
                throw new InvalidUserOrPasswordException(string.Empty);
            }

            if (user.Password != Crypt.Encrypt(userRedirectRequest.Password, user.PasswordSalt))
            {
                RecordFailedLoginAttempt(user);
                Configuration.MaxFailedLoginAttemptsErrorMessage = string.Format(Configuration.MaxFailedLoginAttemptsErrorMessage, Configuration.MaxFailedLoginAttempts);
                throw new InvalidUserOrPasswordException(user.Username, $"{Configuration.MaxFailedLoginAttemptsErrorMessage}");
            }

            if (user.FailedLoginAttempts > 0)
                ResetFailedLoginAttempts(user);

            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();
            ValidateScopeAccess(scope, user.Username, userRedirectRequest.Portal);

            var userRedirect = await UserRedirectRepository.Get(userRedirectRequest.Username);
            var redirectionToken = $"{Guid.NewGuid():N}{Guid.NewGuid():N}";

            if (userRedirect == null)
            {
                userRedirect = new UserRedirect();
                userRedirect.UserId = user.Id;
                userRedirect.Username = userRedirectRequest.Username;
                userRedirect.RedirectionUrl = userRedirectRequest.RedirectUrl;
                userRedirect.RedirectionToken = redirectionToken;
                userRedirect.RedirectionTokenCreatedTime = TenantTime.Now;
                userRedirect.RedirectionTokenUpdatedTime = TenantTime.Now;
                userRedirect.RedirectionTokenExpirationTime = TenantTime.Now.AddMinutes(Configuration.RedirectionTokenTimeout);
                userRedirect.RedirectionTokenIsValid = true;

                await UserRedirectRepository.Add(userRedirect);
            }
            else
            {
                userRedirect.RedirectionUrl = userRedirectRequest.RedirectUrl;
                userRedirect.RedirectionToken = redirectionToken;
                userRedirect.RedirectionTokenUpdatedTime = TenantTime.Now;
                userRedirect.RedirectionTokenExpirationTime = TenantTime.Now.AddMinutes(Configuration.RedirectionTokenTimeout);
                userRedirect.RedirectionTokenIsValid = true;

                UserRedirectRepository.Update(userRedirect);
            }

            await EventHub.Publish("UserRedirectionTokenGenerated", new UserRedirectionEvent(userRedirect.UserId, userRedirect.Username, Convert.ToString(userRedirect.RedirectionTokenUpdatedTime)));

            return await Task.FromResult(redirectionToken);
        }


        /// <summary>
        /// Create a token, this is required by HUB for a short term solution as request by Allen on April 05
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<string> GetTokenUsingLOCredentials(string username)
        {
            var user = await UserRepository.Get(username);

            //// If user does not exist in database
            if (user == null)
            {
                throw new InvalidUserOrPasswordException(username);
            }
            
            if(!user.Roles.Any(Configuration.HubTokenGenerationAllowedForRoles.Contains))
                throw new Exception("Generation supported only for Borrower");

            //// If account is locked
            if (IsAccountLocked(user))
            {
                Configuration.AccountLockoutMessage = string.Format(Configuration.AccountLockoutMessage, Configuration.AccountLockoutPeriod);
                throw new AccountLockedException(user.Username, Configuration.AccountLockoutMessage ?? $"Account is locked for {user.Username}");
            }

            if (HasReachedMaxFailedLoginAttempts(user) && !IsInLockoutPeriod(user))
                user.FailedLoginAttempts = 0;

            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();
            string rawToken = await CreateToken(scope, user.Username);

            return rawToken;
        }

        public async Task<UserRedirectResponse> ValidateRedirectionToken(string redirectionToken)
        {
            if (string.IsNullOrEmpty(redirectionToken))
                throw new ArgumentNullException(nameof(redirectionToken));

            var userRedirect = await UserRedirectRepository.GetByRedirectionToken(redirectionToken);

            if (userRedirect == null)
            {
                throw new RedirectTokenInValidException(redirectionToken);
            }

            if (!userRedirect.RedirectionTokenIsValid)
            {
                throw new RedirectTokenInValidException(redirectionToken);
            }

            if ((userRedirect.RedirectionTokenExpirationTime.Value - TenantTime.Now).TotalMinutes < 0)
            {
                userRedirect.RedirectionTokenIsValid = false;
                UserRedirectRepository.Update(userRedirect);
                throw new RedirectTokenTimeoutException(redirectionToken);
            }

            var user = await UserRepository.Get(userRedirect.Username);

            if (user == null)
                throw new InvalidUserOrPasswordException(userRedirect.Username);

            if (!user.IsActive)
                throw new ArgumentException("User is not active.");

            if (IsAccountLocked(user))
            {
                Configuration.AccountLockoutMessage = string.Format(Configuration.AccountLockoutMessage, Configuration.AccountLockoutPeriod);
                throw new AccountLockedException(user.Username, Configuration.AccountLockoutMessage ?? $"Invalid login credentials for {user.Username}");
            }

            if (HasReachedMaxFailedLoginAttempts(user) && !IsInLockoutPeriod(user))
                user.FailedLoginAttempts = 0;

            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();
            string rawToken = await CreateToken(scope, user.Username);
            await UserRepository.UpdateLastLogin(user.Username, TenantTime.Now);
            await EventHub.Publish("UserLoggedIn", new UserRedirectionEvent(userRedirect.UserId, userRedirect.Username, Convert.ToString(userRedirect.RedirectionTokenUpdatedTime)));

            return new UserRedirectResponse()
            {
                UserId = userRedirect.UserId,
                Token = rawToken,
                Redirecturl = userRedirect.RedirectionUrl
            };
        }


        public async Task<ILoginResponse> LoginWithSignInToken(string source,string signInToken)
        {
            if (string.IsNullOrEmpty(signInToken))
                throw new ArgumentNullException(nameof(signInToken));

            if (string.IsNullOrEmpty(source))
                throw new ArgumentNullException(nameof(source));

            EnsureSourceIsValid(source);

            var user = await UserRepository.GetBySignInToken(signInToken);

             if (user == null)
                throw new ArgumentException(Configuration.UserNotFoundErrorMessage ?? "User not found for supplied token.");

            if (!user.IsActive)
                throw new ArgumentException(Configuration.UserIsNotActiveErrorMessage ?? "User is not active.");

            if (IsAccountLocked(user))
            {
                Configuration.AccountLockoutMessage = string.Format(Configuration.AccountLockoutMessage, Configuration.AccountLockoutPeriod);
                throw new AccountLockedException(user.Username, Configuration.AccountLockoutMessage ?? $"Invalid login credentials for {user.Username}");
            }

            if (HasReachedMaxFailedLoginAttempts(user) && !IsInLockoutPeriod(user))
                user.FailedLoginAttempts = 0;
            

             if (user.DirectSignInTokenExpiration < TenantTime.Now.DateTime)
                 throw new ArgumentException(Configuration.TokenExpiredErrorMessage ?? "Token is expired.");
            

            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();

           var loginResponse = await UserLogin(user,scope);          
           return loginResponse;
        }


        public async Task<bool> MapToParentUser(string userName , string ParentUserName)
        {   
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

            if (string.IsNullOrEmpty(ParentUserName))
                throw new ArgumentNullException(nameof(ParentUserName));

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            if (!user.IsActive)
                throw new ArgumentException("{0} is not active.",userName);
            var parentUser = await UserRepository.Get(ParentUserName);

            if (parentUser == null)
                throw new UserNotFoundException(ParentUserName);

             if (!user.IsActive)
                throw new ArgumentException("{0} is not active.",ParentUserName);            
            

            await UserRepository.MapToParentUser(userName, ParentUserName);
            return true;
        }

        public async Task<ICollection<IUserInfo>> GetAllChildUser(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            if (!user.IsActive)
                throw new ArgumentException("{0} is not active.",userName);
            var childUser = await UserRepository.GetAllChildUser(userName);
            return childUser.Select(x => new UserDetail(x)).ToList<IUserInfo>();            
        }

        public async Task<ICollection<IUserInfo>> GetAllChildUserWithRole(string userName,string role)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));
            
            if (string.IsNullOrEmpty(role))
                throw new ArgumentNullException(nameof(role));
            
            var roleName = GetRoleByName(Configuration.Roles, role);
             if (string.IsNullOrEmpty(roleName.Key))
                throw new ArgumentException("'" + role + "' role does not exist.");

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            if (!user.IsActive)
                throw new ArgumentException("{0} is not active.",userName);
            var childUser = await UserRepository.GetAllChildUserWithRole(userName,role);
            var childUserList = childUser.Select(x => new UserDetail(x)).ToList<IUserInfo>();
            return childUserList;
        }

        public async Task<IUserInfo> GetParentUser(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            var parentUser = await UserRepository.Get(user.ParentUserName);

            if (parentUser == null)
                throw new UserNotFoundException(user.ParentUserName);
            
            return new UserDetail(parentUser);
        }

        public async Task<ICollection<IUserInfo>> GetAllParentUser(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

             List<IUser> parentUserList = new List<IUser>();

             await GetParentUserInfo(user.ParentUserName,parentUserList);

            
            return parentUserList.Select(x => new UserDetail(x)).ToList<IUserInfo>();
        }


        private async Task GetParentUserInfo(string userName, List<IUser> parentUserList)
        {           
            if(!string.IsNullOrEmpty(userName))
            {
                var parentUser = await UserRepository.Get(userName);
                if(parentUser != null) 
                {       
                    parentUserList.Add(parentUser);
                    await GetParentUserInfo(parentUser.ParentUserName,parentUserList);
                }
                else
                    return;

            }
            else 
                return;
        }

        public async Task LockUser(string token)
        {
            var user = await GetUserFromToken(token);
            if(user != null) {
                await GenerateNewPassword(user); 
            }
        }

        private async Task<IUser> GetUserFromToken(string token)
        {  
           IUser objUser = await UserRepository.GetByToken(token);
           return objUser;
        }
        private async Task GenerateNewPassword(IUser objUser)
        {  
            int passwordLength = Convert.ToInt32(Configuration.DefaultPasswordLength);
            var password = await CreatePassword(passwordLength);
            objUser.Password = Crypt.Encrypt(password, objUser.PasswordSalt);
            await UserRepository.ChangePasswordOnly(objUser,objUser.Password);
        }
        private async Task<string> CreatePassword(int length)
        {
            return await Task.Run(() =>
            {
                string strRandomPassword = Configuration.RandomGeneratePassword;
                StringBuilder res = new StringBuilder();
                Random rnd = new Random();
                while (0 < length--)
                {
                    res.Append(strRandomPassword[rnd.Next(strRandomPassword.Length)]);
                }
                return res.ToString();
            });
         
        }


        #endregion        
    }
}