﻿using System.Text.RegularExpressions;

namespace LendFoundry.Security.Identity
{
    public class UserNameValidationRule : IUserNameValidationRule
    {
        public UserNameValidationRule(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        private IConfiguration Configuration { get; set; }

        public bool Complies(string username)
        {
            if (username == null) return false;
            if (string.IsNullOrEmpty(Configuration.UserNameComplexityPattern))
                return true;
            return Regex.IsMatch(username, Configuration.UserNameComplexityPattern);
        }
    }
}
