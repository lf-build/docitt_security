﻿using System;

namespace LendFoundry.Security.Identity
{
    public class AccountLockedException : Exception
    {
        public AccountLockedException(string username, string msg) : base(msg)
        {
            Username = username;
        }

        public string Username { get; }
    }
}
