namespace LendFoundry.Security.Identity
{
    public interface IUserNameValidationRule
    {
        bool Complies(string username);
    }
}
