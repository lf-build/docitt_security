﻿using System.Text.RegularExpressions;

namespace LendFoundry.Security.Identity
{
    public class PasswordComplexityRules : IPasswordComplexityRules
    {
        public PasswordComplexityRules(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; set; }
      
        public bool Complies(string password)
        {
            if (password == null) return false;
            if (string.IsNullOrEmpty(Configuration.PasswordComplexityPattern))
                return true;
            return Regex.IsMatch(password, Configuration.PasswordComplexityPattern);
        }
    }
}
