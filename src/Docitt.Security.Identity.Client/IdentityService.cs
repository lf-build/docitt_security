﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using LendFoundry.Security.Identity.Models;
using LendFoundry.Foundation.Client;
using RestSharp;

namespace LendFoundry.Security.Identity.Client
{
    public class IdentityService : IIdentityService
    {
        public IdentityService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IUserInfo> CreateUser(IUser user)
        {
            return await Client.PutAsync<IUser, UserInfo>($"/", user, true);
        }

        public async Task<IEnumerable<string>> GetUserRoles(string name)
        {
             return await Client.GetAsync<List<string>>($"/roles/user/{name}");
        }

        public async Task<IEnumerable<string>> GetChildRoles(string role)
        {
           return await Client.GetAsync<List<string>>($"/roles/{role}/children");
        }

        public async Task<ICollection<IUser>> GetRoleMembers(string role)
        {
            return await Client.GetAsync<List<IUser>>($"/roles/{role}/members");
        }

        public async Task<ICollection<IUser>> GetRoleMemberList(string role)
        {
            return await Client.GetAsync<List<IUser>>($"/roles/{role}/memberlist");
        }

       public async Task<IEnumerable<string>> GetChildRoles(List<string> roles)
        {
             var uri = $"/roles/children/";
             uri = uri + string.Join("/", roles);
            return await Client.GetAsync<List<string>>(uri);
        }

        public async Task<IEnumerable<string>> GetAllRolesFromCurrentUser()
        {
              return await Client.GetAsync<List<string>>($"/roles/user");
        }

        public async Task<IUser> GetUser(string username)
        {
           return await Client.GetAsync<User>($"/{username}");
        }

        public async Task<IEnumerable<string>> UserRoles(string username, string[] roles)
        {
            return await Client.PutAsync<string[], List<string>>($"/{username}/update-roles", roles, true);
        }

        public async Task ActivateUser(string username)
        {
             await Client.PutAsync<dynamic>($"/activate/{username}", null, true);      
        }

        public async Task DeActivateUser(string username)
        {
            await Client.PutAsync<dynamic>($"/deactivate/{username}", null, true);      
        }

         public async Task<bool> MapToParentUser(string username , string parentusername)
        {
            var request = new RestRequest("/{username}/link-parent/{parentusername}", Method.PUT);
            request.AddUrlSegment(nameof(username), username);
            request.AddUrlSegment(nameof(parentusername), parentusername);
            var result = await Client.ExecuteAsync(request);
            return result;
        }

          public async Task<ICollection<IUserInfo>> GetAllChildUser(string username)
           {
            var request = new RestRequest("/{username}/child-user", Method.GET);
            request.AddUrlSegment(nameof(username), username);
            var result = await Client.ExecuteAsync<ICollection<IUserInfo>>(request);
            return result;
            }

            public async  Task<IUserInfo> GetParentUser(string username)
           {
            var request = new RestRequest("/{username}/parent-user", Method.GET);
            request.AddUrlSegment(nameof(username), username);
            var result = await Client.ExecuteAsync<IUserInfo>(request);
            return result;
            }

            public async Task<ICollection<IUserInfo>> GetAllParentUser(string username)
           {
            var request = new RestRequest("/{username}/parent-user/all", Method.GET);
            request.AddUrlSegment(nameof(username), username);
            var result = await Client.ExecuteAsync<ICollection<IUserInfo>>(request);
            return result;
            }

              public async  Task<ICollection<IUserInfo>> GetAllChildUserWithRole(string username,string role)
           {
            var request = new RestRequest("/child-user/{username}/with-role/{role}", Method.GET);
            request.AddUrlSegment(nameof(username), username);
             request.AddUrlSegment(nameof(role), role);
            var result = await Client.ExecuteAsync<ICollection<IUserInfo>>(request);
            return result;
            }

            public async Task<dynamic> LockUser(string token)
            {
                return await Client.GetAsync<dynamic>($"/lock-user/{token}");
            }
    }
}