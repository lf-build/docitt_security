using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity.Client
{
    public interface IIdentityService
    {
        Task<IUserInfo> CreateUser(IUser user);

        Task<IEnumerable<string>> GetUserRoles(string username);

        Task<IEnumerable<string>> GetChildRoles(string role);

        Task<ICollection<IUser>> GetRoleMembers(string role);

        Task<ICollection<IUser>> GetRoleMemberList(string role);

        Task<IEnumerable<string>> GetChildRoles(List<string> roles);

        Task<IEnumerable<string>> GetAllRolesFromCurrentUser();

        Task<IUser> GetUser(string username);

        Task<IEnumerable<string>> UserRoles(string username, string[] roles);

        Task ActivateUser(string userName);

        Task DeActivateUser(string userName);

         Task<bool> MapToParentUser(string userName , string ParentUserName);

         Task<ICollection<IUserInfo>> GetAllChildUser(string userName);

        Task<IUserInfo> GetParentUser(string userName);

        Task<ICollection<IUserInfo>> GetAllParentUser(string userName);

        Task<ICollection<IUserInfo>> GetAllChildUserWithRole(string userName,string role);

        Task<dynamic> LockUser(string token);

    }
}