using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Security.Identity.Client
{
    public static class IdentityServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddIdentityService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IIdentityServiceFactory>(p => new IdentityServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IIdentityServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
		
		 public static IServiceCollection AddIdentityService(this IServiceCollection services, Uri uri)
        {
             services.AddTransient<IIdentityServiceFactory>(p => new IdentityServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IIdentityServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddIdentityService(this IServiceCollection services)
        {
            services.AddTransient<IIdentityServiceFactory>(p => new IdentityServiceFactory(p));
            services.AddTransient(p => p.GetService<IIdentityServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}