﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Serializers;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;

namespace LendFoundry.Security.Identity.Mongo
{
    public class UserRepository : IUserRepository
    {
        private static ConcurrentDictionary<string, IMongoCollection<User>> Collections = new ConcurrentDictionary<string, IMongoCollection<User>>();

        static UserRepository()
        {
            BsonClassMap.RegisterClassMap<User>(map =>
            {
                map.AutoMap();
                map.MapIdField(m => m.Id).SetIdGenerator(StringObjectIdGenerator.Instance);
                map.MapMember(m => m.LastFailedLoginAttempt).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.LastPasswordModifyOn).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.LastActiveDeactiveOn).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.CreatedOn).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.ModifyOn).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.LastLoggedIn).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
            });
        }

        public UserRepository(IMongoConfiguration configuration, ITenantService tenantService, ITenantTime tenantTime)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tenantService == null)
                throw new ArgumentNullException(nameof(tenantService));

            if (string.IsNullOrEmpty(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrEmpty(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            var key = configuration.ConnectionString + configuration.Database;
            
            IMongoCollection<User> tmpCollection;
            if (!Collections.TryGetValue(key, out tmpCollection))
            {
                var client = new MongoClient(configuration.ConnectionString);
                var database = client.GetDatabase(configuration.Database);
                tmpCollection = database.GetCollection<User>("users");

                tmpCollection.Indexes.CreateOneAsync(Builders<User>.IndexKeys.Ascending(u => u.TenantId).Ascending(u => u.Username), new CreateIndexOptions { Unique = true });
                Collections.TryAdd(key, tmpCollection);
            }

            Collection = tmpCollection;
            
            TenantService = tenantService;
            TenantTime = tenantTime;
        }

        private IMongoCollection<User> Collection { get; }

        private ITenantService TenantService { get; }

        private ITenantTime TenantTime { get; }
        public async Task Add(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            user.Id = ObjectId.GenerateNewId().ToString();

            var entry = new User
            {
                Id = user.Id,
                TenantId = TenantService.Current.Id,
                Email = !string.IsNullOrWhiteSpace(user.Email) ? user.Email.ToLower() : user.Email,
                Name = user.Name,
                Username = user.Username.ToLower(),
                Roles = user.Roles,
                Password = user.Password,
                PasswordSalt = user.PasswordSalt,
                ResetToken = user.ResetToken,
                ResetTokenExpiration = user.ResetTokenExpiration,
                EmailVerificationToken = user.EmailVerificationToken,
                EmailVerificationTokenExpiration = user.EmailVerificationTokenExpiration,
                EmailVerified = user.EmailVerified,
                PhoneVerified = user.PhoneVerified,
                CountryCode = user.CountryCode,
                MobileNumber = user.MobileNumber,
                Portal = user.Portal,
                InvitationReferenceId = user.InvitationReferenceId,
                BrandedSiteReferenceId = user.BrandedSiteReferenceId,
                IsActive = true,
                CreatedOn = DateTime.UtcNow,
                ParentUserName = !string.IsNullOrWhiteSpace(user.ParentUserName) ? user.ParentUserName.ToLower() : user.ParentUserName,
                Source = user.Source,
                DirectSignInToken = user.DirectSignInToken,
                DirectSignInTokenExpiration = user.DirectSignInTokenExpiration,
                DirectSignInUrl = user.DirectSignInUrl
            };

            await Collection.InsertOneAsync(entry);
        }

        public async Task<ICollection<IUser>> All()
        {
            var builder = new FilterDefinitionBuilder<User>();
            var filter = builder.Eq(candidate => candidate.TenantId, TenantService.Current.Id);

            var found = await Collection.FindAsync(filter);
            var users = await found.ToListAsync();
            return new List<IUser>(users);
        }

        public async Task<IUser> Get(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await Collection
                .AsQueryable()
                .FirstOrDefaultAsync(FindByUsername(username));

            return user;
        }
        
        public async Task<IUser> GetByToken(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            var user = await Collection
                .AsQueryable()
                .FirstOrDefaultAsync(FindByToken(token));


            return user;
        }

        public async void Update(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));


            UpdateDefinition<User> definition = null;
            var builder = new UpdateDefinitionBuilder<User>();

            if (!string.IsNullOrEmpty(user.Name))
                definition = builder.Set(u => u.Name, user.Name);

            if (!string.IsNullOrEmpty(user.Email))
                definition = definition.Set(u => u.Email, user.Email);

            if (user.Roles != null && user.Roles.Any())
                definition = definition.Set(u => u.Roles, user.Roles);

            definition = definition.Set(u => u.ModifyOn, DateTime.UtcNow);

            if (definition != null)
                await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task UpdateLastLogin(string name, DateTimeOffset date)
        {
            var userData = await Get(name);
            var builder = new UpdateDefinitionBuilder<User>();
            var definition = builder.Set(u => u.PreviousLoggedIn, userData.LastLoggedIn);
            definition = definition.Set(u => u.LastLoggedIn, date);
            await Collection.UpdateOneAsync(FindByUsername(name), definition);
        }

        public async Task UserActivation(string name, bool isActive)
        {
            var builder = new UpdateDefinitionBuilder<User>();
            var definition = builder.Set(u => u.IsActive, isActive);
            definition = definition.Set(u => u.LastActiveDeactiveOn, DateTime.UtcNow);

            await Collection.UpdateOneAsync(FindByUsername(name), definition);
        }
        public async void Remove(IUserInfo user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            await Collection.DeleteOneAsync(FindByUsername(user.Username));
        }

        public async Task ChangePassword(IUserInfo user, string password)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException(nameof(password));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.Password, password)
                .Set(u => u.ResetToken, string.Empty)
                .Set(u => u.ResetTokenExpiration, null)
                .Set(u => u.LastPasswordModifyOn, DateTime.UtcNow);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task ChangePasswordOnly(IUserInfo user, string password)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException(nameof(password));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.Password, password)
                .Set(u => u.LastPasswordModifyOn, DateTime.UtcNow);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async void SetResetToken(IUser user, string token, DateTime expiration)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            //Now need to convert from UTC time to TenantTime
            //if (expiration.ToUniversalTime() < DateTime.UtcNow)
            if (expiration.ToUniversalTime() < TenantTime.Now.DateTime.ToUniversalTime())
                throw new ArgumentException("Invalid token expiration date. It is in the past.");

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.ResetToken, token)
                .Set(u => u.ResetTokenExpiration, expiration);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task<ICollection<IUser>> AllWithRole(string role)
        {
            var builder = new FilterDefinitionBuilder<User>();
            var tenant = builder.Eq(candidate => candidate.TenantId, TenantService.Current.Id);
            var roles = builder.AnyIn(candidate => candidate.Roles, new[] { role });
            var filter = builder.And(tenant, roles);


            var found = await Collection.FindAsync(filter);
            var users = await found.ToListAsync();
            return new List<IUser>(users);
        }

        public async Task ChangeName(IUserInfo user, string name)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.Name, name);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        private Expression<Func<User, bool>> FindByUsername(string username)
        {
             username = username.ToLower();
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Username == username;
        }

        private Expression<Func<User, bool>> FindByToken(string token)
        {
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.ResetToken == token;
        }
        public async void UpdateFailedLoginAttempts(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.FailedLoginAttempts, user.FailedLoginAttempts)
                .Set(u => u.LastFailedLoginAttempt, user.LastFailedLoginAttempt);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task MapToParentUser(string userName,string parentUserName)
        {   
            parentUserName = parentUserName.ToLower();
            var builder = new UpdateDefinitionBuilder<User>();
            var definition = builder.Set(u => u.ParentUserName, parentUserName);          
            await Collection.UpdateOneAsync(FindByUsername(userName), definition);
        }

        public async Task<ICollection<IUser>> GetAllChildUser(string userName)
        {
            userName = userName.ToLower();
            var builder = new FilterDefinitionBuilder<User>();
            var tenant = builder.Eq(candidate => candidate.TenantId, TenantService.Current.Id);
            var parentUser = builder.Eq(candidate => candidate.ParentUserName, userName);
            var filter = builder.And(tenant, parentUser);

            var found = await Collection.FindAsync(filter);
            var users = await found.ToListAsync();
            return new List<IUser>(users);
           
//            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IUser, User>(); });
//            return config.CreateMapper().Map<List<User>, List<IUser>>(users);           
        }

        public async Task<ICollection<IUser>> GetAllChildUserWithRole(string userName,string role)
        {
            userName = userName.ToLower();
            var builder = new FilterDefinitionBuilder<User>();
            var tenant = builder.Eq(candidate => candidate.TenantId, TenantService.Current.Id);
            var parentUser = builder.Eq(candidate => candidate.ParentUserName, userName);
              var roles = builder.AnyIn(candidate => candidate.Roles, new[] { role });
            var filter = builder.And(tenant, parentUser,roles);

            var found = await Collection.FindAsync(filter);
            var users = await found.ToListAsync();
            return new List<IUser>(users);
//            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IUser, User>(); });
//            return config.CreateMapper().Map<List<User>, List<IUser>>(users);

        }


        public async Task<IUser> GetBySignInToken(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            var user = await Collection
                .AsQueryable()
                .FirstOrDefaultAsync(FindBySignInToken(token));


            return user;
        }

        public async Task ExpireSignInToken(IUser user)
        {
            var definition = new UpdateDefinitionBuilder<User>()            
                .Set(u => u.DirectSignInTokenExpiration, TenantTime.Now.DateTime);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }


         private Expression<Func<User, bool>> FindBySignInToken(string token)
        {
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.DirectSignInToken == token;
        }
    }
}
