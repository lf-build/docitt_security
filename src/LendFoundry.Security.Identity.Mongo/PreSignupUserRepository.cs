﻿using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;

namespace LendFoundry.Security.Identity.Mongo
{
    public class PreSignupUserRepository : IPreSignupUserRepository
    {
        private static ConcurrentDictionary<string, IMongoCollection<User>> Collections = new ConcurrentDictionary<string, IMongoCollection<User>>();

        public PreSignupUserRepository(IMongoConfiguration configuration, ITenantService tenantService)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tenantService == null)
                throw new ArgumentNullException(nameof(tenantService));

            if (string.IsNullOrEmpty(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrEmpty(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            var key = configuration.ConnectionString + configuration.Database;
            IMongoCollection<User> tmpCollection;
            if (!Collections.TryGetValue(key, out tmpCollection))
            {
                var client = new MongoClient(configuration.ConnectionString);
                var database = client.GetDatabase(configuration.Database);
                tmpCollection = database.GetCollection<User>("preusersignup");

                tmpCollection.Indexes.CreateOneAsync(Builders<User>.IndexKeys.Ascending(u => u.TenantId).Ascending(u => u.Username), new CreateIndexOptions { Unique = true });
                Collections.TryAdd(key, tmpCollection);
            }

            Collection = tmpCollection;            
            TenantService = tenantService;
        }

        private IMongoCollection<User> Collection { get; }

        private ITenantService TenantService { get; }

        public async Task Add(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            user.Id = ObjectId.GenerateNewId().ToString();

            var entry = new User
            {
                Id = user.Id,
                TenantId = TenantService.Current.Id,
                Email = !string.IsNullOrWhiteSpace(user.Email) ? user.Email.ToLower() : user.Email,
                Name = user.Name,
                Username = user.Username.ToLower(),
                Roles = user.Roles,
                Password = user.Password,
                PasswordSalt = user.PasswordSalt,
                ResetToken = user.ResetToken,
                ResetTokenExpiration = user.ResetTokenExpiration,
                EmailVerificationToken = user.EmailVerificationToken,
                EmailVerificationTokenExpiration = user.EmailVerificationTokenExpiration,
                MobileNumber = user.MobileNumber,
                Portal = user.Portal,
                InvitationReferenceId = user.InvitationReferenceId

            };

            await Collection.InsertOneAsync(entry);
        }

        public async void Update(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            var builder = new UpdateDefinitionBuilder<User>();

            var definition = builder.Set(u => u.EmailVerified, user.EmailVerified);
            definition = definition.Set(u => u.PhoneVerified, user.PhoneVerified);

            if (!string.IsNullOrEmpty(user.MobileNumber))
                definition = definition.Set(u => u.MobileNumber, user.MobileNumber);

            if (!string.IsNullOrEmpty(user.CountryCode))
                definition = definition.Set(u => u.CountryCode, user.CountryCode);

            if (definition != null)
                await Collection.UpdateOneAsync(FindByUserid(user.Id), definition);
        }

        public async Task<IUser> Get(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await Collection
                .AsQueryable()
                .FirstOrDefaultAsync(FindByUsername(username));

            return user;
        }

        public async Task<IUser> GetById(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            var user = await Collection
                .AsQueryable()
                .FirstOrDefaultAsync(FindByUserid(id));

            return user;
        }
        private Expression<Func<User, bool>> FindByUsername(string username)
        {
            username = username.ToLower();
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Username == username;
        }

        private Expression<Func<User, bool>> FindByUserid(string uid)
        {
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Id == uid;
        }


    }
}
