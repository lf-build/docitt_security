using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity.Mongo
{
    public class User : IUser
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string[] Roles { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string ResetToken { get; set; }
        public DateTime? ResetTokenExpiration { get; set; }
        public int FailedLoginAttempts { get; set; }
        public DateTimeOffset? LastFailedLoginAttempt { get; set; }

        public string EmailVerificationToken { get; set; }

        public DateTime? EmailVerificationTokenExpiration { get; set; }

        public bool EmailVerified { get; set; }

        public bool PhoneVerified { get; set; }

        public string CountryCode { get; set; }
        public string MobileNumber { get; set; }

        public string Portal { get; set; }

        public Dictionary<string, object> settings { get; set; }


        public DateTimeOffset? LastPasswordModifyOn { get; set; }

        public string InvitationReferenceId { get; set; }

        public string BrandedSiteReferenceId { get; set; }

        public bool IsActive { get; set; }     

        public DateTimeOffset? LastActiveDeactiveOn { get; set; }

        public DateTimeOffset? CreatedOn { get; set; }

        public DateTimeOffset? ModifyOn { get; set; }

        public List<string> Permissions { get; set; }

        public DateTimeOffset? LastLoggedIn
        {
            get;

            set;
        }

        public DateTimeOffset? PreviousLoggedIn
        {
            get;

            set;
        }

        public string ParentUserName { get; set;}

         public string Source {get;set;}

       
        public string DirectSignInUrl {get;set;} 

        public string DirectSignInToken {get;set;}

         public DateTime?  DirectSignInTokenExpiration  { get; set; }
    }
}