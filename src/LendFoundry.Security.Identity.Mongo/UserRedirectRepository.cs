﻿using System;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Serializers;

namespace LendFoundry.Security.Identity.Mongo
{
    public class UserRedirectRepository : IUserRedirectRepository
    {
        private static ConcurrentDictionary<string, IMongoCollection<UserRedirect>> Collections = new ConcurrentDictionary<string, IMongoCollection<UserRedirect>>();

        #region "Constructors"

        static UserRedirectRepository()
        {
            BsonClassMap.RegisterClassMap<UserRedirect>(map =>
            {
                map.AutoMap();
                map.MapIdField(m => m.Id).SetIdGenerator(StringObjectIdGenerator.Instance);
                map.MapMember(m => m.RedirectionTokenCreatedTime).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.RedirectionTokenUpdatedTime).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.RedirectionTokenExpirationTime).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
            });
        }

        public UserRedirectRepository(IMongoConfiguration configuration, ITenantService tenantService)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tenantService == null)
                throw new ArgumentNullException(nameof(tenantService));

            if (string.IsNullOrEmpty(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrEmpty(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            var key = configuration.ConnectionString + configuration.Database;
            IMongoCollection<UserRedirect> tmpCollection;
            if (!Collections.TryGetValue(key, out tmpCollection))
            {
                var client = new MongoClient(configuration.ConnectionString);
                var database = client.GetDatabase(configuration.Database);
                tmpCollection = database.GetCollection<UserRedirect>("userredirect");

                tmpCollection.Indexes.CreateOneAsync(Builders<UserRedirect>.IndexKeys.Ascending(u => u.TenantId).Ascending(u => u.UserId), new CreateIndexOptions { Unique = true });
                tmpCollection.Indexes.CreateOneAsync(Builders<UserRedirect>.IndexKeys.Ascending(u => u.TenantId).Ascending(u => u.Username), new CreateIndexOptions { Unique = true });

                Collections.TryAdd(key, tmpCollection);
            }

            Collection = tmpCollection;


            TenantService = tenantService;

        }

        #endregion

        #region "Properties and Methods"

        private IMongoCollection<UserRedirect> Collection { get; }
        private ITenantService TenantService { get; }

        private Expression<Func<UserRedirect, bool>> FindByUsername(string username)
        {
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Username == username;
        }

        private Expression<Func<UserRedirect, bool>> FindByUserId(string username)
        {
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Username == username;
        }

        private Expression<Func<UserRedirect, bool>> FindByRedirectionToken(string redirectionToken)
        {
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.RedirectionToken == redirectionToken;
        }

        #endregion

        #region "Methods"

        public async Task<IUserRedirect> Get(string username = "", string userId = null)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            UserRedirect user;

            if (!string.IsNullOrEmpty(userId))
            {
                user = await Collection
                    .AsQueryable()
                    .FirstOrDefaultAsync(FindByUserId(userId));
            }
            else
            {
                user = await Collection
                    .AsQueryable()
                    .FirstOrDefaultAsync(FindByUsername(username));
            }

            return user;
        }

        public async Task<IUserRedirect> GetByRedirectionToken(string redirectionToken)
        {
            if (string.IsNullOrEmpty(redirectionToken))
                throw new ArgumentNullException(nameof(redirectionToken));

            return await Collection
                    .AsQueryable()
                    .FirstOrDefaultAsync(FindByRedirectionToken(redirectionToken));
        }

        public async Task Add(IUserRedirect user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            user.Id = ObjectId.GenerateNewId().ToString();

            var entry = new UserRedirect
            {
                Id = user.Id,
                TenantId = TenantService.Current.Id,
                UserId = user.UserId,
                Username = user.Username,
                RedirectionUrl = user.RedirectionUrl,
                RedirectionToken = user.RedirectionToken,
                RedirectionTokenCreatedTime = user.RedirectionTokenCreatedTime,
                RedirectionTokenUpdatedTime = user.RedirectionTokenUpdatedTime,
                RedirectionTokenExpirationTime = user.RedirectionTokenExpirationTime,
                RedirectionTokenIsValid = user.RedirectionTokenIsValid
            };

            await Collection.InsertOneAsync(entry);
        }

        public async void Update(IUserRedirect user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            UpdateDefinition<UserRedirect> definition = null;
            var builder = new UpdateDefinitionBuilder<UserRedirect>();

            if (!string.IsNullOrEmpty(user.RedirectionUrl))
                definition = builder.Set(u => u.RedirectionUrl, user.RedirectionUrl);

            if (!string.IsNullOrEmpty(user.RedirectionToken))
                definition = definition.Set(u => u.RedirectionToken, user.RedirectionToken);

            if (user.RedirectionTokenUpdatedTime != null)
                definition = definition.Set(u => u.RedirectionTokenUpdatedTime, user.RedirectionTokenUpdatedTime);

            if (user.RedirectionTokenExpirationTime != null)
                definition = definition.Set(u => u.RedirectionTokenExpirationTime, user.RedirectionTokenExpirationTime);

            definition = definition.Set(u => u.RedirectionTokenIsValid, user.RedirectionTokenIsValid);

            if (definition != null)
                await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        #endregion
    }
}