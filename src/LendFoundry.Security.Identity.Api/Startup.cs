﻿using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.Tenant.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Identity.Mongo;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Email.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Lookup.Client;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using  LendFoundry.Foundation.ServiceDependencyResolver;
namespace LendFoundry.Security.Identity.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Identity"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });

                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Security.Identity.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#endif

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddEmailService();
             services.AddLookupService();
            

            services.AddTransient<ICrypt, Pbkdf2Crypt>();
            services.AddTransient<ITenantTime, TenantTime>();
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ISessionManager, SessionManager>();
            services.AddTransient<IUserRedirectRepository, UserRedirectRepository>();
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<IRoleToPermissionConverter, RoleToPermissionConverter>();
            services.AddTransient<IConfiguration>(p => p.GetService<IConfigurationService<Configuration>>().Get());
            services.AddTransient<IPasswordComplexityRules, PasswordComplexityRules>();
            services.AddTransient<IUserNameValidationRule, UserNameValidationRule>();
            services.AddTransient<IPreSignupUserRepository, PreSignupUserRepository>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Identity Service");
            });

#endif
            app.UseErrorHandling();
            app.UseRequestLogging(new RequestLoggingMiddlewareOptions(new[] { "Password", "CurrentPassword", "ConfirmPassword" }));
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}
