﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Threading.Tasks;
using MongoDB.Driver;
using LendFoundry.Security.Identity.Models;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Security.Identity.Api.Controllers
{
    /// <summary>
    /// API
    /// </summary>
    [Route("/")]
    public class ApiController : Controller
    {
        #region "Constructor"

        /// <summary>
        /// ApiController
        /// </summary>
        /// <param name="identityService"></param>
        /// <param name="logger"></param>
        /// <param name="tokenReader"></param>
        /// <param name="tokenParser"></param>
        public ApiController(IIdentityService identityService, ILogger logger, ITokenReader tokenReader,
            ITokenHandler tokenParser, ITenantTime tenantTime)
        {
            IdentityService = identityService;
            Logger = logger;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            TenantTime = tenantTime;
        }

        #endregion

        #region "Properties"

        private IIdentityService IdentityService { get; }

        private ILogger Logger { get; }

        private ITokenReader TokenReader { get; }

        private ITenantTime TenantTime { get; }

        private IActionResult GetStatusCodeResult(int statusCode)
        {
#if DOTNET2
            return new StatusCodeResult(statusCode);
#else
            return new HttpStatusCodeResult(statusCode);
#endif
        }


        /// <summary>
        /// TokenParser
        /// </summary>
        public ITokenHandler TokenParser { get; set; }

        #endregion

        #region "Methods"

        private static List<string> SplitRoles(string roles)
        {
            return string.IsNullOrWhiteSpace(roles)
                ? new List<string>()
                : roles.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        #endregion

        #region "User APIs"

        /// <summary>
        /// Gets the current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/")]
#if DOTNET2
        [ProducesResponseType(typeof(UserDetail), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetCurrentUser()
        {
            try
            {
                var currentUser = await IdentityService.GetCurrentUser();
                return Ok(new UserDetail(currentUser));
            }
            catch (AuthenticationException)
            {
                return ErrorResult.NotFound("User not authenticated.");
            }
        }

        /// <summary>
        /// GetUser
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>IUser</returns>
        [HttpGet("/{username}")]
#if DOTNET2
        [ProducesResponseType(typeof(IUser), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 401)]
        [ProducesResponseType(500)]
#endif
        public async Task<IActionResult> GetUser(string username)
        {
            try
            {
                if(!string.IsNullOrEmpty(username))
                    username = username.ToLower();

                var user = await IdentityService.GetUser(username);
                return Ok(new UserDetail(user));
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.NotFound("User not found.");
            }
            catch (AuthenticationException)
            {
                return ErrorResult.NotFound("User not authenticated.");
            }
            catch (Exception)
            {
                return ErrorResult.InternalServerError("Server error.Try later.");
            }
        }

        /// <summary>
        /// GetAllRoles
        /// </summary>
        /// <returns></returns>
        [HttpGet("/roles/all")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
#endif
        public async Task<IActionResult> GetAllRoles()
        {
            return Ok(await IdentityService.GetAllRoles());
        }

        /// <summary>
        /// GetAllRolesFromCurrentUser
        /// </summary>
        /// <returns>string[]</returns>
        [HttpGet("/roles/user")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 401)]
#endif
        public async Task<IActionResult> GetAllRolesFromCurrentUser()
        {
            try
            {
                return Ok(await IdentityService.GetAllRolesFromCurrentUser());
            }
            catch (AuthenticationException)
            {
                return ErrorResult.NotFound("User not authenticated.");
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// GetAllChildRolesFromCurrentUser
        /// </summary>
        /// <returns>string[]</returns>
        [HttpGet("/roles/user/children")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetAllChildRolesFromCurrentUser()
        {
            try
            {
                var roles = await IdentityService.GetAllRolesFromCurrentUser();
                var childRoles = await IdentityService.GetChildrenRoles(roles);
                return Ok(childRoles);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// GetUserRoles
        /// </summary>
        /// <param name="name"></param>
        /// <returns>string[]</returns>
        [HttpGet("/roles/user/{name}")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUserRoles(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return ErrorResult.BadRequest("name is required");
                //name = WebUtility.UrlDecode(name);
                return Ok(await IdentityService.GetUserRoles(name));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// GetAllChildRoles
        /// </summary>
        /// <param name="role">role</param>
        /// <returns>string[]</returns>
        [HttpGet("/roles/{role}/children")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
#endif
        public async Task<IActionResult> GetAllChildRoles(string role)
        {
            if (string.IsNullOrWhiteSpace(role))
                return ErrorResult.BadRequest("Role is required");
            role = WebUtility.UrlDecode(role);
            return Ok(await IdentityService.GetChildRoles(role));
        }

        /// <summary>
        /// GetAllParentRoles
        /// </summary>
        /// <param name="role">role</param>
        /// <returns>string[]</returns>
        [HttpGet("/roles/{role}/parents")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
#endif
        public async Task<IActionResult> GetAllParentRoles(string role)
        {
            if (string.IsNullOrWhiteSpace(role))
                return ErrorResult.BadRequest("Role is required");
            role = WebUtility.UrlDecode(role);
            return Ok(await IdentityService.GetParentRoles(role));
        }

        /// <summary>
        /// GetChildrenRoles
        /// </summary>
        /// <param name="roles">roles</param>
        /// <returns>string[]</returns>
        [HttpGet("/roles/children/{*roles}")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
#endif
        public async Task<IActionResult> GetChildrenRoles(string roles)
        {
            if (string.IsNullOrWhiteSpace(roles))
                return ErrorResult.BadRequest("Value cannot be null or whitespace.", nameof(roles));
            roles = WebUtility.UrlDecode(roles);
            var roleList = SplitRoles(roles);
            return Ok(await IdentityService.GetChildrenRoles(roleList));
        }

        /// <summary>
        /// GetRoleMembers
        /// </summary>
        /// <param name="role">role</param>
        /// <returns>IUser[]</returns>
        [HttpGet("/roles/{role}/members")]
#if DOTNET2
        [ProducesResponseType(typeof(IUser[]), 200)]
#endif
        public async Task<IActionResult> GetRoleMembers(string role)
        {
            if (string.IsNullOrWhiteSpace(role))
                return ErrorResult.BadRequest("Role is required");
            role = WebUtility.UrlDecode(role);
            return Ok(await IdentityService.GetRoleMembers(role));
        }

        /// <summary>
        /// GetRoleMembers
        /// </summary>
        /// <param name="role">role</param>
        /// <returns>IUser[]</returns>
        [HttpGet("/roles/{role}/memberlist")]
#if DOTNET2
        [ProducesResponseType(typeof(IUser[]), 200)]
#endif
        public async Task<IActionResult> GetRoleMemberList(string role)
        {
            if (string.IsNullOrWhiteSpace(role))
                return ErrorResult.BadRequest("Role is required");
            role = WebUtility.UrlDecode(role);
            return Ok(await IdentityService.GetRoleMembersShortData(role));
        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        [HttpGet("/logout")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> Logout()
        {
            try
            {
                await IdentityService.Logout(TokenReader.Read());
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// IsAuthorized
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        [HttpGet("/is-authorized/{*uri}")]
#if DOTNET2
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> IsAuthorized([FromRoute]string uri)
        {
            try
            {
                return await IdentityService.IsAuthorized(TokenReader.Read(), uri)
                    ? GetStatusCodeResult(200)
                    : GetStatusCodeResult(403);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }

        }

        /// <summary>
        /// Checks the username.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>bool</returns>
        [HttpPost("check")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> CheckUsername([FromBody] CheckUsernameRequest request)
        {
            try
            {
                 if(request != null && !string.IsNullOrEmpty(request.Username))
                    request.Username = request.Username.ToLower();

                return Ok(new
                {
                    request.Username,
                    Available = await IdentityService.IsUsernameAvailable(request.Username)
                });
            }
            catch (ArgumentNullException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="login">login reques</param>
        /// <returns>ILoginResponse</returns>
        [HttpPost("/login")]

#if DOTNET2
        [ProducesResponseType(typeof(ILoginResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> Login([FromBody] LoginRequest login)
        {
            var s1time = TenantTime.Now.DateTime;
            Logger.Info($"{TenantTime.Now.DateTime} : Login Controller");
            try
            {                
                if (login == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                var result = await IdentityService.Login(login);
                Logger.Info($"{TenantTime.Now.DateTime} : Login Controller finished with {TenantTime.Now.DateTime-s1time}");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }

            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, $"Invalid Username or Password.");

            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// RequestPasswordReset
        /// </summary>
        /// <param name="requestPasswordResetRequest">requestPasswordResetRequest</param>
        /// <param name="sendEmail">sendemail boolean value(true/false)</param>
        /// <returns></returns>
        [HttpPost("/request-token/{sendEmail?}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> RequestPasswordReset([FromBody] RequestPasswordResetRequest requestPasswordResetRequest, bool sendEmail=true)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(requestPasswordResetRequest.Username))
                    return ErrorResult.BadRequest("name is required");
                if (string.IsNullOrWhiteSpace(requestPasswordResetRequest.Portal))
                    return ErrorResult.BadRequest("portal is required");

                await IdentityService.RequestPasswordReset(requestPasswordResetRequest, sendEmail);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (UserNotFoundException)
            {
                return GetStatusCodeResult(404);
            }
        }

        
        /// <summary>
        /// RequestPasswordReset
        /// </summary>
        /// <param name="requestPasswordResetRequest"></param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException"></exception>
        [HttpPost("/request-token/hub")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ResetBorrowerPasswordByLO([FromBody] RequestPasswordResetRequest requestPasswordResetRequest)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());

                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                IdentityService.ValidateRequestByLOForPasswordReset(requestPasswordResetRequest, token.Scope);

                var response = await IdentityService.RequestPasswordReset(requestPasswordResetRequest, false);
                return Ok(response);
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
            catch (UserNotFoundException)
            {
                return GetStatusCodeResult(404);
            }
        }

        /// <summary>
        /// ResetPassword
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="reset">reset</param>
        /// <returns></returns>
        [HttpPost("/reset-password/{token}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ResetPassword([FromRoute] string token, [FromBody] LoginRequest reset)
        {
            try
            {
                if (reset == null)
                    throw new InvalidUserOrPasswordException(string.Empty);
                await IdentityService.ResetPassword(reset.Username, token, reset.Password);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
            catch (UserNotFoundException)
            {
                return GetStatusCodeResult(404);
            }
        }
        
        /// <summary>
        /// ActivateUser
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>NoContentResult</returns>
        [HttpPut("/activate/{username}")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(404)]
#endif
        public async Task<IActionResult> ActivateUser(string username)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);


                await IdentityService.ActivateUser(username);
                return NoContent();
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (UserNotFoundException)
            {
                return GetStatusCodeResult(404);
            }
        }

        /// <summary>
        /// DeActivateUser
        /// </summary>
        /// <param name="username">username</param>
        /// <returns></returns>
        [HttpPut("/deactivate/{username}")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(404)]
#endif
        public async Task<IActionResult> DeActivateUser(string username)
        {
            try
            {
                if(!string.IsNullOrEmpty(username))
                    username = username.ToLower();

                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                await IdentityService.DeActivateUser(username);
                return NoContent();
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (UserNotFoundException)
            {
                return GetStatusCodeResult(404);
            }
        }

        /// <summary>
        /// CreateUser
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>VerifyTokenResponse</returns>
        [HttpPut("/")]
#if DOTNET2
        [ProducesResponseType(typeof(VerifyTokenResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> CreateUser([FromBody] CreateUserRequest request)
        {
            try
            {
                if(request !=null && !string.IsNullOrEmpty(request.Username))
                    request.Username = request.Username.ToLower(); 

                return Ok(await IdentityService.CreateUser(request));
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, $"Invalid {exception.ParamName}.");
            }
            catch (ArgumentException exception)
            {
                Logger.Warn(exception.Message);
                return new ErrorResult(400, exception.Message);
            }
            catch (UsernameAlreadyExists)
            {
                return ErrorResult.BadRequest($"Username {request.Username} already exists!");
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (MongoWriteException exception)
            {
                Logger.Warn("Mongo exception : ", new { exception.Message });
                return ErrorResult.BadRequest(exception.Message.Contains("duplicate key error collection") ? $"Username {request.Username} already exists!" : exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound($"{exception.Message}({exception.Username})");
            }
        }

        /// <summary>
        /// verifyToken
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>VerifyTokenResponse</returns>
        [HttpPut("/verify-token")]
#if DOTNET2
        [ProducesResponseType(typeof(VerifyTokenResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif

        public async Task<IActionResult> verifyToken([FromBody] VerifyTokenRequest request)
        {
            // refid = reference id = UserId of presignup user collection in identity mongo database.
            try
            {
                var response = await IdentityService.VerifyToken(request.refid, request.token);
                if (response.verified)
                {
                    return Ok(response);
                }
                return Ok(new VerifyTokenResponse() { refid = request.refid, verified = false });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// UpdateInformation
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>UpdateUserInformationResponse</returns>
        [HttpPut("/update")]

#if DOTNET2
        [ProducesResponseType(typeof(UpdateUserInformationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> UpdateInformation([FromBody] UpdateUserInformation request)
        {
            try
            {
                var response = await IdentityService.UpdateUserInformation(request.refid, request.phoneNumber, request.countryCode);
                if (response != null)
                {
                    return Ok(new UpdateUserInformationResponse() { refid = request.refid, success = true, UserInfo = response.userDetail, loginResponse = response.loginResponse });
                }
                return Ok(new UpdateUserInformationResponse() { refid = request.refid, success = false });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                Logger.Warn(ex.Message, new { ex.Data });
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        //[HttpPut("/setting/{key}/{value}")]
        //public async Task<IActionResult> UpdateSetting(string key, string value)
        //{
        //    try
        //    {
        //        return new HttpOkObjectResult(await IdentityService.UpdateSetting(key, value));
        //    }
        //    catch (ArgumentException exception)
        //    {
        //        Logger.Warn(exception.Message, new { exception.ParamName });
        //        return new ErrorResult(400, exception.Message);
        //    }
        //}

        /// <summary>
        /// ChangePassword
        /// </summary>
        /// <param name="reset">reset</param>
        /// <returns></returns>
        [HttpPut("change-password")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> ChangePassword([FromBody] PasswordChangeRequest reset)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (reset == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (reset.Password != reset.ConfirmPassword)
                    throw new ArgumentException("Password and Confirm Password does not match");


                await IdentityService.ChangePassword(token.Subject, reset.CurrentPassword, reset.Password);
                return NoContent();
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
            catch (UserNotFoundException)
            {
                return GetStatusCodeResult(404);
            }
        }

        /// <summary>
        /// ChangeName
        /// </summary>
        /// <param name="user">user</param>
        /// <returns></returns>
        [HttpPut("change-name")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> ChangeName([FromBody] NameChangeRequest user)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (user == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                await IdentityService.ChangeName(token.Subject, user.Name);
                return NoContent();
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (UserNotFoundException)
            {
                return GetStatusCodeResult(404);
            }
        }

        /// <summary>
        /// UserRoles
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="roles">roles</param>
        /// <returns>string[]</returns>
        [HttpPut("/{username}/update-roles")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
#endif
        public async Task<IActionResult> UserRoles([FromRoute]string username, [FromBody] string[] roles)
        {
            try
            {
                return Ok(await IdentityService.UpdateUserRoles(username, roles));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        #endregion

        #region "User Redirect APIs"

        /// <summary>
        /// GetRedirectionToken
        /// </summary>
        /// <param name="userRedirectRequest">userRedirectRequest</param>
        /// <returns>string[]</returns>
        [HttpPost("/redirection")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
#endif
        public async Task<IActionResult> GetRedirectionToken([FromBody] UserRedirectRequest userRedirectRequest)
        {
            // TODO
            try
            {
                if (userRedirectRequest == null ||  string.IsNullOrEmpty(userRedirectRequest.Username) || string.IsNullOrEmpty(userRedirectRequest.Password))
                    throw new InvalidUserOrPasswordException(string.Empty);
                string val = await IdentityService.GetRedirectionToken(userRedirectRequest);
                return Ok(new { hashtoken = val});
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidPortalException exception)
            {
                Logger.Warn("Invalid Portal {Portal}", new { exception.Portal });
                return new ErrorResult(401, $"'{ userRedirectRequest.Portal }' Portal access not allowed");
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid Username or Password for {Username}", new { exception.Username });
                return new ErrorResult(401, $"Invalid Username or Password. {exception.Message}");
            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(401, exception.Message);
            }
        }


        /// <summary>
        /// GetRedirectionToken
        /// </summary>
        /// <param name="redirectionToken">redirectionToken</param>
        /// <returns>string[]</returns>
        [HttpPost("/redirectionlogin")]
#if DOTNET2
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
#endif
        public async Task<IActionResult> ValidateRedirectionToken([FromBody] string redirectionToken)
        {
            try
            {
                if (string.IsNullOrEmpty(redirectionToken))
                    throw new Exception("RedirectionToken is required.");

                return Ok(await IdentityService.ValidateRedirectionToken(redirectionToken));
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }

            catch (RedirectTokenInValidException exception)
            {
                Logger.Warn("Invalid Token {RedirectionToken }", new { exception.RedirectionToken });
                return new ErrorResult(400, $"Invalid Token.");
            }
            catch (RedirectTokenTimeoutException exception)
            {
                Logger.Warn("Token Timeout {RedirectionToken }", new { exception.RedirectionToken });
                return new ErrorResult(400, $"Token Timeout.");
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid Username or Password for {Username}", new { exception.Username });
                return new ErrorResult(400, $"Invalid Username or Password.");
            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// This is a created to provide a temporary solution to generate new borrower token
        /// </summary>
        /// <param name="login">login object with username</param>
        /// <returns></returns>
        [HttpPost("/generate/borrower-token")]
        public async Task<IActionResult> GetTokenUsingLOCredentials([FromBody] LoginRequest login)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());

                if (token == null || string.IsNullOrWhiteSpace(token.Subject) || !token.Scope.Contains("back-office"))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (string.IsNullOrEmpty(login.Username))
                    throw new Exception("Username is required.");

                return Ok(new { Token = await IdentityService.GetTokenUsingLOCredentials(login.Username) });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid Username or Password for {Username}", new { exception.Username });
                return new ErrorResult(400, $"Invalid Username or Password for '{exception.Username}'");
            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
        }
        #endregion

        /// <summary>
        /// Map/link User with parent user
        /// </summary>
        /// <param name="username">Name of the user</param>
        /// <param name="parentusername">name of the parent user</param>
        /// <returns></returns>
        [HttpPut("/{username}/link-parent/{parentusername}")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
        public async Task<IActionResult> MapToParentUser([FromRoute]string username, [FromRoute] string parentusername)
        {
            try
            {
                //return Ok(await IdentityService.MapToParentUser(username, parentusername));
                await IdentityService.MapToParentUser(username, parentusername);
                return NoContent();
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// Get all the child user for supplied user.
        /// </summary>
        /// <param name="username">Name of the user</param>
        /// <returns>Collection of UserDetails</returns>
        [HttpGet("/{username}/child-user")]
        [ProducesResponseType(typeof(ICollection<IUserInfo>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
        public async Task<IActionResult> GetAllChildUser([FromRoute]string username)
        {
            try
            {
                if(!string.IsNullOrEmpty(username))
                    username = username.ToLower();
                return Ok(await IdentityService.GetAllChildUser(username));
                
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// Get Parent user information for supplied user
        /// </summary>
        /// <param name="username">Name of the user</param>
        /// <returns>User details</returns>
        [HttpGet("/{username}/parent-user")]
        [ProducesResponseType(typeof(IUserInfo), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
        public async Task<IActionResult> GetParentUser([FromRoute]string username)
        {
            try
            {
                if(!string.IsNullOrEmpty(username))
                    username = username.ToLower();

                return Ok(await IdentityService.GetParentUser(username));
                
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// Get All Parent user information for supplied user
        /// </summary>
        /// <param name="username">Name of the user</param>
        /// <returns>User details</returns>
        [HttpGet("/{username}/parent-user/all")]
        [ProducesResponseType(typeof(ICollection<IUserInfo>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
        public async Task<IActionResult> GetAllParentUser([FromRoute]string username)
        {
            try
            {
                if(!string.IsNullOrEmpty(username))
                    username = username.ToLower();

                return Ok(await IdentityService.GetAllParentUser(username));
                
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// Get all child user for supplied user with supplied role.
        /// </summary>
        /// <param name="username">Name of the user</param>
        /// <param name="role">role name</param>
        /// <returns>Collection of UserDetails</returns>
        [HttpGet("child-user/{username}/with-role/{role}")]
        [ProducesResponseType(typeof(ICollection<IUserInfo>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
        public async Task<IActionResult> GetAllChildUserWithRole([FromRoute]string username,[FromRoute]string role)
        {
            try
            {
                if(!string.IsNullOrEmpty(username))
                    username = username.ToLower();

                return Ok(await IdentityService.GetAllChildUserWithRole(username,role));
                
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// Lock user
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut("/lock-user/{token}")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> LockUser([FromRoute]string token)
        {
            try
            {
                await IdentityService.LockUser(token);
                return NoContent();
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }            
            catch (UserNotFoundException)
            {
                return GetStatusCodeResult(404);
            }
        }

         /// <summary>
        /// Login with signin token
        /// </summary>
        /// <param name="source">request source</param>
        /// <param name="token">sign in token</param>
        /// <returns>ILoginResponse</returns>
        [HttpPost("/login-with-signintoken/{source}/{token}")]
        [ProducesResponseType(typeof(ILoginResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> LoginWithSignInToken(string source,string token)
        {
            try
            {                
                if (string.IsNullOrEmpty(source))
                    throw new ArgumentNullException("Invalid source.");
                
                if (string.IsNullOrEmpty(token))
                    throw new ArgumentNullException("token required.");

                return Ok(await IdentityService.LoginWithSignInToken(source,token));
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }

            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, $"Invalid Username or Password.");

            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
        }
    }
}
